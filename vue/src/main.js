/* eslint-disable */

//import node modules
import Vue from 'vue'
import router from './router'

import lodash from 'lodash'
import VueLodash from 'vue-lodash/dist/vue-lodash.min'
import moment from "moment";
import VueMomentJS from "vue-momentjs";

import draggable from 'vuedraggable'
import fullCalendar from 'vue-fullcalendar'
import Datetime from 'vue-datetime';
// import app utils
import App from './App'
import Default from './layout/default.vue'
import Reusable from './components/general'
import { prepareVuex } from './store'
import Provider from './util/data/ProviderPlugin'
import {apolloProvider} from './util/data/provider/DataClient/apollo'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

//setup stuff
Vue.config.productionTip = false

let store = prepareVuex()

//register Vue use

Vue.use(VueLodash, lodash)
Vue.use(VueMomentJS, moment)
Vue.component('full-calendar', fullCalendar)
Vue.use(Datetime);
Vue.use(Provider)
if (process.env.NODE_ENV === 'production') {
  Raven
    .config('https://a182436057a04ba99a5ba9a6baa39ead@sentry.io/584922')
    .addPlugin(RavenVue, Vue)
    .install();
  Vue.config.errorHandler = (e) => {
    console.log('sending error')
    console.log(e)
    Raven.captureException(e)
  }
  Vue.config.warnHandler = (e) => {
    console.log('warning error')
    console.log(e)
  }
}

//register components

Vue.component('draggable', draggable)
// register reusable components
Object.keys(Reusable).forEach(key => {
  Vue.component(key, Reusable[key])
})

//blastoff
let vv = new Vue({
  el: '#app',
  router,
  store,
  provide: apolloProvider.provide(),
  template: '<App/>',
  components: {App}
})
