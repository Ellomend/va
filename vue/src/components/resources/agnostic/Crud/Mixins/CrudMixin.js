import List from '../Components/List'
import ListItem from '../Components/ListItem'
import Create from '../Components/Create'
import View from '../Components/View'
import Edit from '../Components/Edit'
import FormPartial from '../Components/FormPartial'
const mixin = {
  props: {
    title: {
      required: false,
      default: 'Crud'
    },
    list: {
      required: true,
      type: Array
    },
    showOnly: {
      required: false,
      type: String
    },
    listComp: {
      required: false
    },
    listItemComp: {
      required: false
    },
    createComp: {
      required: false
    },
    viewComp: {
      required: false
    },
    editComp: {
      required: false
    },
    formComp: {
      required: false
    }
  },
  data () {
    return {
      currentView: 'list',
      viewId: null,
      editId: null
    }
  },
  methods: {
    resetView () {
      this.currentView = 'list'
    },
    setView (view) {
      this.currentView = view
    },

    setViewItem (id) {
      this.setView('view')
      this.viewId = id
    },
    setEditItem (id) {
      this.setView('edit')
      this.editId = id
    },
    doCreate (item) {
      this.resetView()
      this.$emit('crudCreate', item)
    },
    updateItem (item) {
      this.resetView()
      this.$emit('crudUpdate', item)
    },
    removeItem (id) {
      this.resetView()
      this.$emit('crudRemove', id)
    }
  },
  computed: {
    view () {
      return this.showOnly || this.currentView
    },
    viewItem () {
      return this.list.find(item => item.id === this.viewId)
    },
    editItem () {
      return this.list.find(item => item.id === this.editId)
    },
    listCopy () {
      return this.list.map(item => ({...item}))
    },
    listComponent () {
      return this.listComp || List
    },
    listItemComponent () {
      return this.listItemComp || ListItem
    },
    createComponent () {
      return this.createComp || Create
    },
    viewComponent () {
      return this.viewComp || View
    },
    editComponent () {
      return this.editComp || Edit
    },
    formComponent () {
      return this.formComp || FormPartial
    }
  }
}
export default mixin
