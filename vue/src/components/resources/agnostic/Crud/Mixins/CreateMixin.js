const mixin = {
  props: {
    title: {
      type: String,
      required: false,
      default: 'Create'
    },
    formComp: {
      required: true
    }
  },
  data () {
    return {
      item: {
        name: 'new item'
      }
    }
  },
  methods: {
    cancelCreate () {
      this.$emit('cancel')
    },
    doCreate () {
      this.$emit('doCreate', this.item)
    }
  },
  computed: {
    formComponent () {
      return this.formComp
    }
  }
}
export default mixin
