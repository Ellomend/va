const mixin = {
  props: {
    item: {
      required: true
    },
    itemTitle: {
      type: String,
      required: false,
      default: 'name'
    },
    itemId: {
      type: String,
      required: false,
      default: 'id'
    },
    formComp: {
      required: true
    }
  },
  data () {
    return {
      updatedItem: {...this.item}
    }
  },
  methods: {
    close () {
      this.$emit('cancel')
    },
    update () {
      this.$emit('update', this.updatedItem)
    },
    updateItem (item) {
      this.updatedItem = item
    }
  },
  computed: {
    name () {
      return this.item[this.itemTitle]
    },
    id () {
      return this.item[this.itemId]
    },
    formComponent () {
      return this.formComp
    }
  }
}
export default mixin
