const mixin = {
  props: {
    item: {
      required: true
    },
    itemTitle: {
      type: String,
      required: false,
      default: 'name'
    },
    itemId: {
      type: String,
      required: false,
      default: 'id'
    }
  },
  methods: {
    close () {
      this.$emit('cancel')
    },
    edit () {
      this.$emit('edit', this.id)
    },
    doRemove () {
      this.$emit('remove', this.id)
    }
  },
  computed: {
    name () {
      return this.item[this.itemTitle]
    },
    id () {
      return this.item[this.itemId]
    }
  }
}
export default mixin
