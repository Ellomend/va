const mixin = {
  props: {
    value: {
      required: true
    }
  },
  data () {
    return {
      updatedItem: {...this.value}
    }
  },
  methods: {
    update () {
      this.$emit('input', this.updatedItem)
    },
    updateItem (item) {
      this.updatedItem = item
    }
  },
  computed: {
  }
}
export default mixin
