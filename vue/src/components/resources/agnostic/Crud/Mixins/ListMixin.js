import Item from '../Components/ListItem'
const mixin = {
  props: {
    // heading for this component, optional
    title: {
      type: String,
      required: false,
      default: 'CrudList'
    },
    list: {
      required: true
    },
    itemComp: {
      required: false
    }
  },
  methods: {
    viewItem (id) {
      this.$emit('viewItem', id)
    },
    editItem (id) {
      this.$emit('editItem', id)
    },
    removeItem (id) {
      this.$emit('removeItem', id)
    }
  },
  computed: {
    ItemComponent () {
      return this.itemComp || Item
    }
  }
}
export default mixin
