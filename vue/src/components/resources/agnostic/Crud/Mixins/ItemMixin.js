const mixin = {
  props: {
    item: {
      required: true
    },
    itemTitle: {
      type: String,
      required: false,
      default: 'name'
    },
    itemId: {
      type: String,
      required: false,
      default: 'id'
    }
  },
  methods: {
    remove () {
      this.$emit('remove', this.id)
    },
    view () {
      this.$emit('view', this.id)
    },
    edit () {
      this.$emit('edit', this.id)
    }
  },
  computed: {
    name () {
      return this.item[this.itemTitle]
    },
    id () {
      return this.item[this.itemId]
    }
  }
}
export default mixin
