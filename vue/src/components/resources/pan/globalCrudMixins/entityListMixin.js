const mixin = {
  methods: {
    removeItem (id) {
      this.$store.dispatch(this.entityType + 'Store/deleteItem', id)
    },
    editItem (id) {
      this.$store.dispatch('createAndEditModals/openModal', {id: id, type: 'edit', model: this.entityType})
    },
    createItem () {
      console.log('create item')
      this.$store.dispatch('createAndEditModals/openModal', {id: null, type: 'create', model: this.entityType})
    }
  },
  computed: {
    list: function () {
      return this.$store.state[ this.entityType + 'Store' ].list
    }
  },
  created: function () {
    this.$store.dispatch(this.entityType + 'Store/getList')
  }
}
export default mixin
