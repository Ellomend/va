const mixin = {
  data () {
    return {
      list: null
    }
  },
  methods: {
    removeItem (id) {
      this.$store.dispatch(this.entityType + 'Store/deleteItem', id)
    },
    editItem (id) {
      this.$store.dispatch('createAndEditModals/openModal', {id: id, type: 'edit', model: this.entityType})
    },
    createItem () {
      console.log('create item')
      this.$store.dispatch('createAndEditModals/openModal', {id: null, type: 'create', model: this.entityType})
    }
  },
  created: function () {
    // getting list for child entity from parent
    if (this.$route.params.id) {
      this.$Provider.fetchList('point', this.$route.params.id)
        .then(r => {
          console.log(r)
        }
      )
    }
  }
}
export default mixin
