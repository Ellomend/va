import Panel from './Panel.vue'
import Datepicker from '../resources/pan/datepicker.vue'
import dayHeader from './DayHeader.vue'
import basicMask from '../resources/pan/basicEntityMask.vue'
import FullModal from './FullModal.vue'
import DisplayFilters from '../resources/pan/fieldsPartials/displayFilters.vue'
import Dates from '../resources/pan/fieldsPartials/Dates.vue'
import CreateButton from '../resources/pan/buttonsPartials/createButton'
import FiltersField from '../resources/pan/fieldsPartials/FiltersField'
export default {
  Panel,
  Datepicker,
  dayHeader,
  basicMask,
  FullModal,
  DisplayFilters,
  Dates,
  CreateButton,
  FiltersField
}
