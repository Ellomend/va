export const mainDomain = 'http://127.0.0.1:8000'
export const apiDomain = mainDomain + '/api/'
export const loginUrl = mainDomain + '/oauth/token'
export const userUrl = apiDomain + 'user'
export const articlesUrl = apiDomain + 'api/articles'
export const blockUrl = apiDomain + 'api/blocks'

export const getHeader = function () {
  const tokenData = JSON.parse(window.localStorage.getItem('authUser'))
  if (tokenData !== null) {
    const headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + tokenData.access_token
    }
    return headers
  } else {
    return false
  }
}

export const getUserFromStorage = function () {
  return JSON.parse(window.localStorage.getItem('authUserData'))
}
export const getUserIdFromStorage = function () {
  let userData = getUserFromStorage()
  let userId = null
  if (userData.user) {
    userId = userData.user.id
  }
  return userId
}
