/**
 * Created by Alex on 10.06.2017.
 */
// goals
import GoalList from '../../pages/resources/goal/list.vue'
import GoalEdit from '../../pages/resources/goal/edit.vue'
export default [
  {
    path: '/start/goals',
    name: 'start.goals',
    meta: {
      requiresAuth: true
    },
    component: GoalList
  },
  {
    name: 'start.goals.edit',
    path: '/start/goals/:id/edit',
    meta: {
      requiresAuth: true
    },
    component: GoalEdit
  }
]
