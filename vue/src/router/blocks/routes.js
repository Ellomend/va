/**
 * Created by Alex on 10.06.2017.
 */
// blocks
import BlockList from '../../pages/resources/block/list.vue'
export default [
  {
    path: '/start/blocks',
    name: 'start.blocks',
    meta: {
      requiresAuth: true
    },
    component: BlockList
  }
]
