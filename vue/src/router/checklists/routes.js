/**
 * Created by Alex on 10.06.2017.
 */
// checklist
import ChecklistList from '../../pages/resources/checklist/list.vue'
import ChecklistEdit from '../../pages/resources/checklist/edit.vue'
import ChecklistEdit2 from '../../pages/resources/checklist/edit2.vue'
import ChecklistEditRunning from '../../pages/resources/checklist/editRunning.vue'
import ChecklistListExp from '../../pages/resources/checklist/experimentList.vue'
import BasicCrud from '../../pages/resources/checklist/BasicCrud'
export default [
  {
    path: '/start/checklists',
    name: 'start.checklists',
    meta: {
      requiresAuth: true
    },
    component: ChecklistList
  },
  {
    name: 'start.checklists.edit',
    path: '/start/checklists/:id/edit',
    meta: {
      requiresAuth: true
    },
    component: ChecklistEdit
  },
  {
    name: 'start.checklists.edit2',
    path: '/start/checklists/:id/edit2',
    meta: {
      requiresAuth: true
    },
    component: ChecklistEdit2
  },
  {
    name: 'start.checklists.crud',
    path: '/start/checklists/crud',
    meta: {
      requiresAuth: true
    },
    component: BasicCrud
  },
  {
    name: 'start.checklists.editRunning',
    path: '/start/checklists/:id/editrunning',
    meta: {
      requiresAuth: true
    },
    component: ChecklistEditRunning
  },
  {
    name: 'start.checklists.list',
    path: '/start/checklists/experimental',
    meta: {
      requiresAuth: true
    },
    component: ChecklistListExp
  }
]
