/**
 * Created by Alex on 10.06.2017.
 */
// stack
import StackList from '../../pages/resources/stack/list.vue'
import StackEdit from '../../pages/resources/stack/edit.vue'
export default [
  {
    path: '/start/stacks',
    name: 'start.stacks',
    meta: {
      requiresAuth: true
    },
    component: StackList
  },
  {
    name: 'start.stacks.edit',
    path: '/start/stacks/:id/edit',
    meta: {
      requiresAuth: true
    },
    component: StackEdit
  }
]
