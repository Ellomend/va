import Views from '../../pages/overviews/views/index.vue'
import Calendar from '../../pages/overviews/views/calendar/index.vue'
import Table from '../../pages/overviews/views/table/index.vue'

export default [
  {
    path: '/views',
    name: 'views',
    meta: {
      requiresAuth: true
    },
    component: Views,
    children: [
      {
        path: '/views/calendar',
        name: 'views.calendar',
        meta: {
          requiresAuth: true
        },
        component: Calendar
      },
      {
        path: '/views/table',
        name: 'views.table',
        meta: {
          requiresAuth: true
        },
        component: Table
      }
    ]
  }
]

