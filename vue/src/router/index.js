import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '../pages/overviews/LoginPage.vue'
import Dashboard from '../pages/overviews/DashboardPage.vue'
import HomePage from '../pages/overviews/HomePage.vue'
import Start from '../pages/resources/start'
import categoriesRoutes from './categories/routes'
import contextsRoutes from './contexts/routes'
import projectsRoutes from './projects/routes'
import tasksRoutes from './tasks/routes'
import blocksRoutes from './blocks/routes'
import moodsRoutes from './moods/routes'
import goalsRoutes from './goals/routes'
import checklistRoutes from './checklists/routes'
import routineRoutes from './routines/routes'
import stackRoutes from './stacks/routes'
import Views from './views/index'
Vue.use(Router)

// const authUser = JSON.parse(window.localStorage.getItem('authUser'))
const getUserFromStorage = function () {
  return JSON.parse(window.localStorage.getItem('authUser'))
}
let router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/start',
      name: 'start',
      meta: {
        requiresAuth: true
      },
      component: Start
    }
  ]
})
router.addRoutes(categoriesRoutes)
router.addRoutes(contextsRoutes)
router.addRoutes(moodsRoutes)
router.addRoutes(projectsRoutes)
router.addRoutes(blocksRoutes)
router.addRoutes(tasksRoutes)
router.addRoutes(goalsRoutes)
router.addRoutes(checklistRoutes)
router.addRoutes(routineRoutes)
router.addRoutes(stackRoutes)
router.addRoutes(Views)

// router logic
router.beforeEach((to, from, next) => {
  // check if auth is required
  if (to.meta.requiresAuth) {
    // if (authUser && authUser.access_token) {
    //   next()
    // } else {
    //   next({path: '/login'})
    // }
  }
  // redirect from login page if user already logged in
  if (to.name === 'login' && getUserFromStorage && getUserFromStorage.access_token) {
    next({path: '/dashboard'})
  }
  next()
})

export default router
