/**
 * Created by Alex on 10.06.2017.
 */
// routine
import RoutineList from '../../pages/resources/routine/list.vue'
import RoutineEdit from '../../pages/resources/routine/edit.vue'
export default [
  {
    path: '/start/routines',
    name: 'start.routines',
    meta: {
      requiresAuth: true
    },
    component: RoutineList
  },
  {
    name: 'start.routines.edit',
    path: '/start/routines/:id/edit',
    meta: {
      requiresAuth: true
    },
    component: RoutineEdit
  }
]
