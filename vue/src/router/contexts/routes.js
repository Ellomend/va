/**
 * Created by Alex on 10.06.2017.
 */
// contexts
import ContextList from '../../pages/resources/context/list.vue'
export default [
  {
    path: '/start/contexts',
    name: 'start.contexts',
    meta: {
      requiresAuth: true
    },
    component: ContextList
  }
]
