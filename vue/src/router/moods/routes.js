
// moods
import MoodList from '../../pages/resources/mood/list.vue'
export default [
  {
    path: '/start/moods',
    name: 'start.moods',
    meta: {
      requiresAuth: true
    },
    component: MoodList
  }
]
