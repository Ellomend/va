/**
 * Created by Alex on 10.06.2017.
 */
// projects
import ProjectList from '../../pages/resources/project/list.vue'
export default [
  {
    path: '/start/projects',
    name: 'start.projects',
    meta: {
      requiresAuth: true
    },
    component: ProjectList
  }
]
