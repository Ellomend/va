/**
 * Created by Alex on 10.06.2017.
 */
// categories
import CategoryList from '../../pages/resources/category/list.vue'
export default [
  {
    path: '/start/categories',
    name: 'start.categories',
    meta: {
      requiresAuth: true
    },
    component: CategoryList
  }
]
