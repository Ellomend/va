/**
 * Created by Alex on 10.06.2017.
 */
// tasks
import TaskList from '../../pages/resources/task/list.vue'
import TaskFilters from '../../pages/resources/task/filters.vue'
export default [
  {
    path: '/start/tasks',
    name: 'start.tasks',
    meta: {
      requiresAuth: true
    },
    component: TaskList
  },
  {
    name: 'start.tasks.filters',
    path: '/start/tasks/filters',
    meta: {
      requiresAuth: true
    },
    component: TaskFilters
  }
]
