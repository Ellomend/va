const state = {
  errors: []
}

const mutations = {
  addError (state, error) {
    state.errors.push(error)
  },
  removeError (state, error) {
    state.errors.splice(state.errors.indexOf(error), 1)
  }
}

const actions = {
  addError: (context, error) => {
    context.commit('addError', error)
  }
}
const getters = {
  getErrors (state) {
    return state.errors
  }
}

export default {
  state, mutations, actions, getters
}
