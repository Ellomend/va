const state = {
  // {type: create/edit, model: 'task', id: id}
  modal: null
}

const mutations = {
  openModal (state, modal) {
    state.modal = modal
  },
  closeModals (state) {
    state.modal = null
  }
}

const actions = {
  openModal: ({commit}, entity) => {
    commit('openModal', entity)
  },
  closeModals: ({commit}) => {
    commit('closeModals')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
