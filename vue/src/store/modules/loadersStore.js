const state = {
  loading: []
}

const mutations = {
  addLoader (state, loader) {
    state.loading.push(loader)
  },
  removeLoader (state, loader) {
    state.loading.splice(state.loading.indexOf(loader), 1)
  }
}

const actions = {
  addLoader: ({commit}, loader) => {
    commit('addLoader', loader)
  },
  removeLoader: ({commit}, loader) => {
    commit('removeLoader', loader)
  }
}

export default {
  state, mutations, actions
}
