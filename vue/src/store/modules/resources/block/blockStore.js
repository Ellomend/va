import Vue from 'vue'
import Vuex from 'vuex'

import provider from '@/util/data/provider/provider'

Vue.use(Vuex)
let moduleActions = provider.modules.filters.models.blocks.actions
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter((i) => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (block) {
    let hashKey = addLoader(block)
    moduleActions.fetchList().then(response => {
      removeLoader(block, hashKey)
      let results = response
      block.commit('GET_LIST', results)
    })
  },
  addItem: function (block, item) {
    let hashKey = addLoader(block)
    moduleActions.add(item).then(response => {
      removeLoader(block, hashKey)
      block.dispatch('getList')
    })
  },
  deleteItem: function (block, item) {
    let hashKey = addLoader(block)
    moduleActions.remove(item).then(response => {
      removeLoader(block, hashKey)
      block.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (block, item) {
    let hashKey = addLoader(block)
    moduleActions.update(item).then(() => {
      removeLoader(block, hashKey)
      block.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
