import { pluckOnlyIds, prepareEntity } from '../../../../helpers/storeHelper'

export default {
  pluckOnlyIds, prepareTask: prepareEntity
}
