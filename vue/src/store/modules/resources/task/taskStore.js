import Vue from 'vue'
import Vuex from 'vuex'
import * as moment from 'moment'
import provider from '@/util/data/provider/provider'
Vue.use(Vuex)
let moduleActions = provider.modules.tasks.actions()
let filters = provider.modules.timeTable.filters
// todo why tho
let filters1 = provider.modules.timeTable.filters
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter(i => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    moduleActions.fetchList().then(response => {
      let results = response
      removeLoader(context, hashKey)
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.add(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.remove(item).then(response => {
      context.commit('DELETE_ITEM', item)
      removeLoader(context, hashKey)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.update(item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    if (!state.pulled) {
      return null
    }
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },
  getFiltered: function (state, getters, rootState, rootGetters) {
    // let tasks = state.list
    // let filters = rootGetters['filterStore/allFilters']
    // let dates = rootGetters['datesStore/getDates']
    // if (!tasks.length || !Object.keys(filters).length) {
    //   return []
    // }
    // let allTasks = filterByAllFilters(tasks, filters)
    // allTasks = filterByDates(allTasks, dates)
    // return allTasks
    return state.list
  },
  getForToday: function (state, getters, rootState, rootGetters) {
    // let tasks = state.list
    // let filters = rootGetters['filterStore/allFilters']
    // let dates = rootGetters['datesStore/getDates']
    // dates = _.cloneDeep(dates)
    // dates.start = moment().format('YYYY-MM-DD')
    // dates.end = moment().format('YYYY-MM-DD')
    // if (!tasks.length || !Object.keys(filters).length) {
    //   return []
    // }
    // let allTasks = filterByAllFilters(tasks, filters)
    // allTasks = filterByDates(allTasks, dates)
    // return allTasks
    return state.list
  },
  getByDate: (state, getters, rootState, rootGetters) => (date) => {
    let filteredTasks = getters.getFiltered
    let dates = {}
    dates.start = moment().format('YYYY-MM-DD')
    dates.end = moment().format('YYYY-MM-DD')
    filteredTasks = filters.filterByDateRange(filteredTasks, dates.start, dates.end)
    // todo refactor to use updated filters
    console.log(filteredTasks)
    return state.list
  },
  getForThisWeek: function (state, getters, rootState, rootGetters) {
    let filteredTasks = getters.getFiltered
    let dates = {}
    dates.start = moment().startOf('isoWeek')
    dates.end = moment().endOf('isoWeek')
    filteredTasks = filters.getOnlyWithDates(filteredTasks)
    filteredTasks = filters.filterByDateRange(filteredTasks, dates.start, dates.end)
    // todo refactor to use updated filters
    console.log(filteredTasks)
    return state.list
  },
  getForThisMonth: function (state, getters, rootState, rootGetters) {
    let filteredTasks = getters.getFiltered
    filteredTasks = filters1.getOnlyWithDates(filteredTasks)
    let dates = rootGetters['datesStore/getDates']
    if (!dates) {
      return []
    }
    if (!dates || !dates.start || !dates.end) {
      console.log('dates are not set')
      return []
    }
    dates.start = moment().startOf('month')
    dates.end = moment().endOf('month')
    const parser = provider.getModule('parser')()
    parser.entities = filteredTasks
    parser.addProcessor('filterByDates', dates)
    let filters = rootGetters['filterStore/getFilters']
    parser.addProcessor('filterByFilters', filters)
    return parser.process()
  },
  getProcessed: function (state, getters, rootState, rootGetters) {
    let filteredTasks = getters.getAll
    const parser = provider.getModule('parser')()
    parser.entities = filteredTasks
    // we have empty processors
    // first lets get dates from store
    let dates = rootGetters['datesStore/getDates']
    if (!dates) {
      return []
    }
    if (!dates || !dates.start || !dates.end) {
      console.log('dates are not set')
      return []
    }
    // now adding filter
    parser.addProcessor('filterByDates', dates)
    // now lets add filters filter ^^
    let filters = rootGetters['filterStore/getFilters']
    parser.addProcessor('filterByFilters', filters)
    return parser.process()
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
