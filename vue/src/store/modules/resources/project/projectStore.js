import Vue from 'vue'
import Vuex from 'vuex'
import provider from '@/util/data/provider/provider'

Vue.use(Vuex)
let moduleActions = provider.modules.filters.models.projects.actions
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter((i) => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (project) {
    let hashKey = addLoader(project)
    moduleActions.fetchList().then(response => {
      removeLoader(project, hashKey)
      let results = response
      project.commit('GET_LIST', results)
    })
  },
  addItem: function (project, item) {
    let hashKey = addLoader(project)
    moduleActions.add(item).then(response => {
      removeLoader(project, hashKey)
      project.dispatch('getList')
    })
  },
  deleteItem: function (project, item) {
    let hashKey = addLoader(project)
    moduleActions.remove(item).then(response => {
      removeLoader(project, hashKey)
      project.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (project, item) {
    let hashKey = addLoader(project)
    moduleActions.update(item).then(() => {
      removeLoader(project, hashKey)
      project.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
