import Vue from 'vue'
import Vuex from 'vuex'
import provider from '@/util/data/provider/provider'

Vue.use(Vuex)
let moduleActions = provider.modules.filters.models.moods.actions
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter((i) => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (mood) {
    let hashKey = addLoader(mood)
    moduleActions.fetchList().then(response => {
      removeLoader(mood, hashKey)
      let results = response
      mood.commit('GET_LIST', results)
    })
  },
  addItem: function (mood, item) {
    let hashKey = addLoader(mood)
    moduleActions.add(item).then(response => {
      removeLoader(mood, hashKey)
      mood.dispatch('getList')
    })
  },
  deleteItem: function (mood, item) {
    let hashKey = addLoader(mood)
    moduleActions.remove(item).then(response => {
      removeLoader(mood, hashKey)
      mood.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (mood, item) {
    let hashKey = addLoader(mood)
    moduleActions.update(item).then(() => {
      removeLoader(mood, hashKey)
      mood.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
