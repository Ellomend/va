import Vue from 'vue'
import Vuex from 'vuex'
import provider from '@/util/data/provider/provider'

let moduleActions = provider.modules.filters.models.categories.actions
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
Vue.use(Vuex)

const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter((i) => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (category) {
    let hashKey = addLoader(category)
    moduleActions.fetchList().then(response => {
      removeLoader(category, hashKey)
      let results = response
      category.commit('GET_LIST', results)
    })
  },
  addItem: function (category, item) {
    let hashKey = addLoader(category)
    moduleActions.add(item).then(response => {
      removeLoader(category, hashKey)
      category.dispatch('getList')
    })
  },
  deleteItem: function (category, item) {
    let hashKey = addLoader(category)
    moduleActions.remove(item).then(response => {
      removeLoader(category, hashKey)
      category.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (category, item) {
    let hashKey = addLoader(category)
    moduleActions.update(item).then(() => {
      removeLoader(category, hashKey)
      category.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
