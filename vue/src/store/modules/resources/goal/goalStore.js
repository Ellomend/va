import Vue from 'vue'
import Vuex from 'vuex'
import provider from '@/util/data/provider/provider'

Vue.use(Vuex)
let moduleActions = provider.modules.goals.actions()
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter(i => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    moduleActions.fetchList().then(response => {
      removeLoader(context, hashKey)
      let results = response
      console.log('res')
      console.log(results)
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.add(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.remove(item).then(response => {
      removeLoader(context, hashKey)
      context.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.update(item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },
  getFiltered: function (state, getters, rootState, rootGetters) {
    let filteredGoals = state.list
    const parser = provider.getModule('parser')()
    parser.entities = filteredGoals

    let filters = rootGetters['filterStore/getFilters']
    parser.addProcessor('filterByFilters', filters)
    return parser.process()
  },
  getFilteredSteps (state, getters, rootState, rootGetters) {
    let goals = getters['getFiltered']
    let steps = []
    goals.map(goal => {
      goal.steps.map(step => {
        steps.push(step)
      })
    })
    return steps
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
