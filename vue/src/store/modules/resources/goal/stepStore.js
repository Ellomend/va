import Vue from 'vue'
import Vuex from 'vuex'
import * as _ from 'lodash/collection'
import provider from '@/util/data/provider/provider'

// import { getUserIdFromStorage } from '@/config'

Vue.use(Vuex)
let moduleActions = provider.modules.goals.actions()
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter(i => {
      return i.id !== item.id
    })
  }
}

const actions = {
  async getList (context, payload) {
    console.log('payload')
    console.log(payload)
    console.log(context)
    let hashKey = addLoader(context)
    // let userId = getUserIdFromStorage()
    moduleActions.fetchSteps(payload).then(response => {
      console.log('steps response')
      console.log(response)
      removeLoader(context, hashKey)
      let results = response
      console.log('steps result')
      console.log(results)
      context.commit('GET_LIST', results)
    })
  },
  async addItem (context, payload) {
    let hashKey = addLoader(context)
    console.log('create payload')
    console.log(payload)
    // let userId = getUserIdFromStorage()
    moduleActions.createStep(payload).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('goalStore/getList', null, {root: true})
    })
  },
  deleteItem: function (context, payload) {
    let hashKey = addLoader(context)
    moduleActions.removeStep(payload).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('goalStore/getList', null, {root: true})
    })
  },
  updateItem: function (context, payload) {
    let hashKey = addLoader(context)
    delete payload.goal
    moduleActions.updateStep(payload).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('goalStore/getList', null, {root: true})
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByGoal: (state, getters) => (id) => {
    let steps = _.filter(state.list, step => {
      console.log('step', step)
      return step.goal_id === id
    })
    return steps
  },
  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    let step = state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
    // step = prepareEntity(step)
    let step2 = {...step}
    return step2
  },
  getFiltered: function (state, getters, rootState, rootGetters) {
    return state.list
    // let steps = state.list
    // debugger
    // let filters = rootGetters['filterStore/allFilters']
    // if (!steps || !steps.length || !Object.keys(filters).length) {
    //   return []
    // }
    // let allSteps = filterByAllFilters(steps, filters)
    // return allSteps
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
