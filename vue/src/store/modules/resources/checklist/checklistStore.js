import Vue from 'vue'
import Vuex from 'vuex'
import provider from '@/util/data/provider/provider'

import * as _ from 'lodash'

Vue.use(Vuex)
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
let moduleActions = provider.modules.checklists.actions()

const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list = state.list.filter(i => {
      return i.id !== item.id
    })
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    moduleActions.fetchList().then(response => {
      removeLoader(context, hashKey)
      let results = response.map(item => {
        let newItem = {...item}
        newItem.type = parseInt(item.type)
        return newItem
      })
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.add(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.remove(item).then(response => {
      removeLoader(context, hashKey)
      context.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.update(item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  startChecklist: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.startChecklist(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },
  getBlueprints: function (state) {
    let list = _.filter(state.list, (item) => {
      return item.type === 0
    })
    return list
  },
  getRunning: function (state) {
    return _.filter(state.list, (item) => {
      return item.type === 1
    })
  },
  getFilteredRunning: function (state, getters, rootState, rootGetters) {
    let filteredTasks = getters.getRunning
    const parser = provider.getModule('parser')()
    parser.entities = filteredTasks

    let filters = rootGetters['filterStore/getFilters']
    parser.addProcessor('filterByFilters', filters)
    return parser.process()
  },
  getFilteredBlueprints: function (state, getters, rootState, rootGetters) {
    let filteredTasks = getters.getBlueprints
    const parser = provider.getModule('parser')()
    parser.entities = filteredTasks

    let filters = rootGetters['filterStore/getFilters']
    parser.addProcessor('filterByFilters', filters)
    return parser.process()
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
