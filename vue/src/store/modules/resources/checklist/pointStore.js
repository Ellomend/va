import Vue from 'vue'
import Vuex from 'vuex'
import * as _ from 'lodash/collection'
import provider from '@/util/data/provider/provider'

Vue.use(Vuex)
let checklists = provider.repository.getModel('Checklists')
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list.splice(state.list.indexOf(item), 1)
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    checklists.getPoints('point').then(response => {
      removeLoader(context, hashKey)
      let results = response.data.data
      results = _.orderBy(results, ['order'])
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    checklists.addPoint(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('checklistStore/getList', null, {root: true})
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    checklists.removePoint(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('checklistStore/getList', null, {root: true})
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    checklists.updatePoint(item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('checklistStore/getList', null, {root: true})
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByChecklist: (state, getters) => (id) => {
    let points = _.filter(state.list, function (point) {
      return point.checklist_id === Number(id)
    })
    points = _.sortBy(points, ['order'])
    return points
  },
  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    let point = state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
    // point = prepareEntity(point)
    return point
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
