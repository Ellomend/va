import Vue from 'vue'
import Vuex from 'vuex'
import Web from 'Helpers/web'
import * as _ from 'lodash/collection'
import provider from '@/util/data/provider/provider'

Vue.use(Vuex)
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list.splice(state.list.indexOf(item), 1)
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    Web.getItems('chore').then(response => {
      removeLoader(context, hashKey)
      let results = response.data.data
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.addItem('chore', item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.removeItem('chore', item).then(response => {
      removeLoader(context, hashKey)
      context.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.updateItem('chore', item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByRoutine: (state, getters) => (id) => {
    let chores = _.filter(state.list, function (chore) {
      return chore.routine_id === Number(id)
    })
    return chores
  },
  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    let chore = state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
    // chore = prepareEntity(chore)
    return chore
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
