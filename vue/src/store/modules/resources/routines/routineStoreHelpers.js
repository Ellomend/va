import later from 'later'
import moment from 'moment'

later.date.localTime()
export const addIsReadyMethod = function (routines) {
  let result = routines.map(item => {
    item = {...item}
    item.isReady = isReady
    item.getNext = getNext
    item.getDiffToNow = getDiffToNow
    item.getStartDate = getStartDate
    item.getNextTen = getNextTen
    item.getLastComplete = getLastComplete
    item.diffToNow = diffToNow
    item.getLastCompleteDiff = getLastCompleteDiff
    item.getNextDiff = getNextDiff
    return item
  })
  return result
}
/* assess if next occurrence is in past */
export const isReady = function () {
  if (!this.getLastComplete()) {
    return true
  }
  let next = this.getNext()
  let now = moment()
  return now.isAfter(next)
}
export const getNext = function () {
  let occurrences = this.getNextTen()
  let res = occurrences[0]
  if (moment(res).isBefore(moment())) {
    res = occurrences[1]
  }
  return res
}
function getNextDiff () {
  return this.getDiffToNow(this.getNext())
}
export const getDiffToNow = function () {
  return moment(this.getNextTen()[0]).fromNow()
}

function getStartDate (momentType = false) {
  let startDate = this.getLastComplete()
  if (momentType) {
    startDate = moment(startDate)
  }
  return startDate
}

function getNextTen () {
  let shedText = this.shed ? this.shed : '{}'
  let shedObj = later.parse.text(shedText)
  return later.schedule(shedObj).next(10, this.getStartDate())
}

function getLastComplete () {
  return this.last_complete ? this.last_complete : null
}
function getLastCompleteDiff () {
  return this.diffToNow(this.getLastComplete())
}
function diffToNow (time) {
  return moment(time).fromNow()
}
