import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'
import * as _ from 'lodash'
import { addIsReadyMethod } from './routineStoreHelpers'
import provider from '@/util/data/provider/provider'
let moduleActions = provider.modules.routines.actions()
let addLoader = provider.modules.helpers.addLoader
let removeLoader = provider.modules.helpers.removeLoader
Vue.use(Vuex)

const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list.splice(state.list.indexOf(item), 1)
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    moduleActions.fetchList().then(response => {
      removeLoader(context, hashKey)
      let results = addIsReadyMethod(response)
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.add(item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    moduleActions.remove(item).then(response => {
      removeLoader(context, hashKey)
      context.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    console.log('beforue update r')
    console.log(item)
    moduleActions.update(item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  completeRoutine (context, routineId) {
    let hashKey = addLoader(context)
    let routine = _.find(context.state.list, item => {
      return item.id === routineId
    })
    routine.last_complete = moment().format('YYYY-MM-DD HH:mm:ss')
    console.log('before update routine')
    console.log(routine)
    moduleActions.updateWithObjects(routine).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    console.log('stt', state.list, id)
    return state.list.find(item => {
      return id === item.id
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
