import Vue from 'vue'
import Vuex from 'vuex'
import Web from 'Helpers/web'
import * as _ from 'lodash/collection'
import { addLoader, removeLoader } from '../../../../helpers/storeHelper'

Vue.use(Vuex)

const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list.splice(state.list.indexOf(item), 1)
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    Web.getItems('stackitem').then(response => {
      removeLoader(context, hashKey)
      let results = response.data.data
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.addItem('stackitem', item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.removeItem('stackitem', item).then(response => {
      removeLoader(context, hashKey)
      context.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.updateItem('stackitem', item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByStack: (state, getters) => (id) => {
    let rItems = _.filter(state.list, function (rItem) {
      return rItem.stack_id === Number(id)
    })
    return rItems
  },
  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    let rItem = state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
    // rItem = prepareEntity(rItem)
    return rItem
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
