import Vue from 'vue'
import Vuex from 'vuex'
import Web from 'Helpers/web'
import { addLoader, prepareEntity, removeLoader } from '../../../../helpers/storeHelper'
Vue.use(Vuex)

const state = {
  list: [],
  pulled: false
}

const mutations = {
  GET_LIST (state, list) {
    state.pulled = true
    state.list = list
  },
  ADD_ITEM (state, item) {
    state.list.push(item)
  },
  DELETE_ITEM (state, item) {
    state.list.splice(state.list.indexOf(item), 1)
  }
}

const actions = {
  getList: function (context) {
    let hashKey = addLoader(context)
    Web.getItems('stack').then(response => {
      removeLoader(context, hashKey)
      let results = response.data.data
      context.commit('GET_LIST', results)
    })
  },
  addItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.addItem('stack', item).then(response => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  },
  deleteItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.removeItem('stack', item).then(response => {
      removeLoader(context, hashKey)
      context.commit('DELETE_ITEM', item)
    })
  },
  updateItem: function (context, item) {
    let hashKey = addLoader(context)
    Web.updateItem('stack', item).then(() => {
      removeLoader(context, hashKey)
      context.dispatch('getList')
    })
  }
}

const getters = {
  getAll: function (state) {
    return state.list
  },
  getById: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    return state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
  },

  getByIdWithIds: (state, getters) => (id) => {
    if (!state.pulled) {
      return null
    }
    let stack = state.list.find(item => {
      if ((item.id + '') === (id + '')) {
        return true
      }
    })
    stack = prepareEntity(stack)
    return stack
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
