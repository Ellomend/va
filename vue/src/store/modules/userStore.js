/*
 * Copyright (c) 2017.
 * Ellomend.ru
 * All rights reserved.
 */
// import { loginUrl, userUrl, getHeader } from '@/config'
// import apolloClient from '/apollo.js'
import apolloClient from '@/util/data/provider/DataClient/apollo'
// import axios from 'axios'
import gql from 'graphql-tag'
const state = {
  authUser: null
}

const mutations = {
  SET_AUTH_USER (state, userObj) {
    state.authUser = userObj
  },
  DELETE_AUTH_USER (state) {
    state.authUser = null
  }
}

const actions = {
  setUserObject: ({commit}, userObj) => {
    commit('SET_AUTH_USER', userObj)
  },
  deleteUserObject: ({commit}) => {
    commit('DELETE_AUTH_USER')
    window.localStorage.removeItem('authUser')
  },
  performLogin: ({commit, rootState, dispatch}, credentials) => {
    const authUserToken = {}
    let authUserData = {}
    let hashKey = Date.now()
    dispatch('addLoader', hashKey, {root: true})
    apolloClient.mutate({
      mutation: gql`
          mutation SigninUserMutation($email: String!, $password: String!) {
              signinUser(email: {
                  email: $email,
                  password: $password
              }) {
                  token
                  user {
                      id
                  }
              }
          }
      `,
      variables: {
        email: credentials.email,
        password: credentials.password
      }
    }).then((result) => {
      // const id = result.data.signinUser.user.id
      // const token = result.data.signinUser.token
      authUserToken.access_token = result.data.signinUser.token
      authUserData = result.data.signinUser
      window.localStorage.setItem('authUser', JSON.stringify(authUserToken))
      window.localStorage.setItem('authUserData', JSON.stringify(authUserData))
      dispatch('removeLoader', hashKey, {root: true})
      dispatch('setUserObject', authUserData)
    }).catch((error) => {
      dispatch('removeLoader', hashKey, {root: true})
      alert(error)
      let errorMsg = 'Unable login, status: ' + error.response.status + ', ' + error.response.statusText + '.'
      commit('addError', errorMsg)
    })
    // axios.post(loginUrl, postData).then(response => {
    //   if (response.status === 200) {
    //     authUserToken.access_token = response.data.access_token
    //     authUserToken.refresh_token = response.data.refresh_token
    //     window.localStorage.setItem('authUser', JSON.stringify(authUserToken))
    //     window.axios.get(userUrl, {headers: getHeader()})
    //       .then(response => {
    //         authUserData.email = response.data.email
    //         authUserData.name = response.data.name
    //         window.localStorage.setItem('authUserData', JSON.stringify(authUserData))
    //         dispatch('setUserObject', authUserData)
    //       })
    //   }
    //   dispatch('removeLoader', hashKey, {root: true})
    // }).catch(function (error) {
    //   let errorMsg = 'Unable login, status: ' + error.response.status + ', ' + error.response.statusText + '.'
    //   commit('addError', errorMsg)
    // })
  }
}

export default {
  state, mutations, actions
}
