const state = {
}

const mutations = {
}

const actions = {
  removeEntity: (context, entity) => {
    context.dispatch(entity.modelName + 'Store/deleteItem', entity, { root: true })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
