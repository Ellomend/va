import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'
// import { apiDomain, getHeader } from '@/config'
import provider from '@/util/data/provider/provider'
import { modelsFactory } from '@/util/data/provider/DataModels/modelsFactory'
import BlockModel from '@/util/data/provider/DataModels/Filters/Blocks/BlockModel'
import CategoryModel from '@/util/data/provider/DataModels/Filters/Categories/CategoryModel'
import ProjectModel from '@/util/data/provider/DataModels/Filters/Projects/ProjectModel'
import ContextModel from '@/util/data/provider/DataModels/Filters/Contexts/ContextModel'
import MoodModel from '@/util/data/provider/DataModels/Filters/Moods/MoodModel'

Vue.use(Vuex)
let moduleActions = provider.modules.filters.models.custom.actions
let filterActions = provider.modules.filters.actions
const state = {
  unitSettings: {
    show: {
      categories: true,
      moods: true,
      projects: true,
      contexts: true,
      blocks: true
    }
  },
  filters: []
}

const mutations = {
  hideFilter (state, filter) {
    state.unitSettings.show[filter] = false
  },
  showFilter (state, filter) {
    state.unitSettings.show[filter] = true
  },
  toggleFilter (state, selectedFilter) {
    state.filters = filterActions.toggleFilter(selectedFilter, state.filters)
  },
  resetAll (state) {
    state.filters = []
  }
}

const actions = {
  showFilter: ({commit}, filter) => {
    commit('showFilter', filter)
  },
  hideFilter: ({commit}, filter) => {
    commit('hideFilter', filter)
  },
  toggleShowFilter: ({commit}, filter) => {
    commit('toggleFilter', filter)
  },
  fetchFilters: (state) => {
    let hashKey = Date.now()
    state.dispatch('addLoader', hashKey, {root: true})
    moduleActions.fetchFilters().then(response => {
      response = {
        'blocks': modelsFactory(response.data.User.blocks, BlockModel),
        'categories': modelsFactory(response.data.User.categories, CategoryModel),
        'projects': modelsFactory(response.data.User.projects, ProjectModel),
        'contexts': modelsFactory(response.data.User.contexts, ContextModel),
        'moods': modelsFactory(response.data.User.moods, MoodModel)
      }
      state.dispatch('blockStore/getList', response.blocks, {root: true})
      state.dispatch('projectStore/getList', response.projects, {root: true})
      state.dispatch('contextStore/getList', response.contexts, {root: true})
      state.dispatch('moodStore/getList', response.moods, {root: true})
      state.dispatch('categoryStore/getList', response.categories, {root: true})
      state.dispatch('removeLoader', hashKey, {root: true})
    })
  },
  toggleFilter: ({commit}, filter) => {
    commit('toggleFilter', filter)
  },
  resetAll: ({commit}) => {
    commit('resetAll')
  }
}

const getters = {
  allFilters: (state, getters, rootState, rootGetters) => {
    let filters = {
      categories: rootGetters['categoryStore/getAll'],
      blocks: rootGetters['blockStore/getAll'],
      contexts: rootGetters['contextStore/getAll'],
      projects: rootGetters['projectStore/getAll'],
      moods: rootGetters['moodStore/getAll']
    }
    return filters
  },
  getDates: (state, getters, rootState, rootGetters) => {
    return rootGetters['dates/getDates']
  },
  getFilters: (state) => {
    return state.filters
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
