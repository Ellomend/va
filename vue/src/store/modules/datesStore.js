const state = {
  time: {
    start: null,
    end: null,
    showLate: true,
    showDateless: true
  }
}

const mutations = {
  // dates constrains
  setDates (state, array) {
    console.log('mutation')
    console.log(array)
    state.time.start = array[0]
    state.time.end = array[1]
  },
  setShowLate (state, val) {
    state.time.showLate = val
  },
  setShowDateless (state, val) {
    state.time.showDateless = val
  },
  setTime (state, val) {
    state.time = val
  }
}

const actions = {
  setDates: ({commit}, datesArray) => {
    commit('setDates', datesArray)
  },
  setShowLate: ({commit}, val) => {
    commit('setShowLate', val)
  },
  setShowDateless: ({commit}, val) => {
    commit('setShowDateless', val)
  },
  setTime: ({commit}, val) => {
    commit('setTime', val)
  }
}

const getters = {
  getDates: state => {
    return state.time
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
