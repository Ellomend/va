/*
 * Copyright (c) 2017.
 * Ellomend.ru
 * All rights reserved.
 */

import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)
// import user module
import userStore from './modules/userStore'
import errorsStore from './modules/errorsStore'
import filterStore from './modules/filtersStore'
import datesStore from './modules/datesStore'

// resources stores
import taskStore from './modules/resources/task/taskStore'
import contextStore from './modules/resources/context/contextStore'
import goalStore from './modules/resources/goal/goalStore'
import stepStore from './modules/resources/goal/stepStore'
import checklistStore from './modules/resources/checklist/checklistStore'
import pointStore from './modules/resources/checklist/pointStore'
import routineStore from './modules/resources/routines/routineStore'
import categoryStore from './modules/resources/category/categoryStore'
import moodStore from './modules/resources/mood/moodStore'
import projectStore from './modules/resources/project/projectStore'
import blockStore from './modules/resources/block/blockStore'
import loadersStore from './modules/loadersStore'
import createAndEditModals from './modules/createAndEditModals'
import panResourcesStuff from './modules/panResourcesStuff'

export const prepareVuex = function () {
  const vuex = new Vuex.Store({
    modules: {
      taskStore,
      userStore,
      errorsStore,
      filterStore,
      datesStore,
      goalStore,
      stepStore,
      checklistStore,
      pointStore,
      routineStore,
      categoryStore,
      projectStore,
      moodStore,
      blockStore,
      contextStore,
      loadersStore,
      createAndEditModals,
      panResourcesStuff
    },
    plugins: [createPersistedState({
      paths: ['datesStore', 'filterStore']
    })]
  })

  return vuex
}

