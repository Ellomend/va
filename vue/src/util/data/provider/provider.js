import DataModelsRepository from '@/util/data/provider/DataModels/DataModelsRepository'
import Tasks from './Modules/Tasks'
import Goals from './Modules/Goals'
import Routines from './Modules/Routines'
import Checklists from './Modules/Checklists'
import Filters from './Modules/Filters'
import TimeTable from './Modules/TimeTable'
import Parser from './Modules/Parser'
import Helpers from './Modules/Helpers'
export const Provider = {
  name: 'provider',
  repository: DataModelsRepository,
  modules: {
    tasks: Tasks,
    goals: Goals,
    routines: Routines,
    checklists: Checklists,
    filters: Filters,
    timeTable: TimeTable,
    parser: Parser,
    helpers: Helpers
  },
  getModule (name) {
    return this.modules[name]
  },
  fetchList (modelName, id = null) {

  }
}
export default Provider
