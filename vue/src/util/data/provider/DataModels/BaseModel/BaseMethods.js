import { ISENTITYATTR } from '@/util/data/provider/DataModels/Constants'

export const fieldAttributes = [
  ISENTITYATTR
]
const hasFieldAttribute = function (field) {
  return this.fieldAttributes.includes(field)
}
export default {
  fieldAttributes,
  hasFieldAttribute
}
