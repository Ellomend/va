import { COMPLETED_ARRAY, PENDING_ARRAY } from '@/util/data/provider/DataModels/Constants'

const getStatus = function () {
  return this.status
}
const isPending = function () {
  return !!PENDING_ARRAY.includes(this.status)
}
const isComplete = function () {
  return !!COMPLETED_ARRAY.includes(this.status)
}

export default {
  getStatus,
  isPending,
  isComplete
}
