import methods from '@/util/data/provider/DataModels/Checklists/methods'
import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'
import { CHILDRENSATTR, FILTERSATTR } from '@/util/data/provider/DataModels/Constants'

export const ChecklistModel = {
  ...BaseModel,
  modelName: 'checklist',
  modelMethods: {
    ...methods
  },
  modelAttributes: {
  },
  fieldAttributes: [
    CHILDRENSATTR,
    FILTERSATTR
  ]
}
export default ChecklistModel
