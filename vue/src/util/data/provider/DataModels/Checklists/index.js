import Client from '@/util/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import {
  ADD_POINT,
  CHECKLISTS_POINTS,
  CREATE_CHECKLIST, DELETE_CHECKLIST, UPDATE_CHECKLIST,
  USER_CHECKLISTS, REMOVE_POINT, GET_POINT_BY_ID, UPDATE_POINT, CLONE_CHECKLIST, SET_POINT_STATUS, USER_POINTS,
  GET_CHECKLIST_BY_ID
} from '@/util/data/provider/DataModels/Checklists/queries'
import { modelsFactory } from '../modelsFactory'
import { ChecklistModel } from '@/util/data/provider/DataModels/Checklists/ChecklistModel'
import PointModel from '@/util/data/provider/DataModels/Checklists/PointModel'
import {convertRelationsToIds} from '@/util/data/provider/DataModels/modelsFactory'
import {orderPointByOrder} from '@/util/data/provider/DataModels/Checklists/methods'

export const Checklists = {
  name: 'Checklists',
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_CHECKLISTS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    let checklists = resp.data.User.checklists.map(checklist => {
      checklist = {...checklist}
      checklist = orderPointByOrder(checklist)
      checklist.points = modelsFactory(checklist.points, PointModel)
      return checklist
    })
    return modelsFactory(checklists, ChecklistModel)
  },
  async getChecklistById (id) {
    console.log('id', id)
    let resp = await Client.query({
      query: GET_CHECKLIST_BY_ID,
      variables: {
        id: id
      },
      fetchPolicy: 'network-only'
    })
    let checklists = [resp.data.Checklist].map(checklist => {
      checklist = {...checklist}
      checklist = orderPointByOrder(checklist)
      checklist.points = modelsFactory(checklist.points, PointModel)
      return checklist
    })
    let checklist = checklists[0]
    checklist = modelsFactory([checklist], ChecklistModel)
    return checklist[0]
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_CHECKLIST,
      variables: {
        name: payload.name,
        type: payload.type || 0,
        userId: userId,
        description: payload.description,
        ...convertRelationsToIds(payload)
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_CHECKLIST,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    console.log('checklist update', payload)
    let resp = await Client.mutate({
      mutation: UPDATE_CHECKLIST,
      variables: {
        name: payload.name,
        description: payload.description,
        id: payload.id,
        ...convertRelationsToIds(payload),
        type: payload.type || 0
      }
    })
    return resp
  },
  async fetchChecklistPoints (checklistId) {
    let resp = await Client.query({
      query: CHECKLISTS_POINTS,
      variables: {
        id: checklistId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.Checklist.points, PointModel)
  },
  async getPoints () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_POINTS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.checklists.points, PointModel)
  },
  async addPoint (point) {
    let resp = await Client.mutate({
      mutation: ADD_POINT,
      variables: {
        ...point
      }
    })
    return resp.data
  },
  async removePoint (point) {
    let resp = await Client.mutate({
      mutation: REMOVE_POINT,
      variables: {
        id: point.id
      }
    })
    return resp
  },
  async getPointById (id) {
    let resp = await Client.query({
      query: GET_POINT_BY_ID,
      variables: {
        id: id
      }
    })
    return resp.data.Point
  },
  async updatePoint (point) {
    let resp = await Client.mutate({
      mutation: UPDATE_POINT,
      variables: {
        ...point
      }
    })
    return resp
  },
  pointUp (point) {
    point.order = point.order + 1
    return this.updatePoint(point)
  },
  pointDown (point) {
    point.order = point.order - 1
    return this.updatePoint(point)
  },
  async startChecklist (checklist) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let newChecklist = {...checklist}
    delete newChecklist.id
    newChecklist.points = newChecklist.points.map(point => {
      point = {
        name: point.name,
        description: point.description,
        order: point.order,
        status: point.status,
        checklist: point.checklist
      }
      delete point.id
      delete point.__typename
      return point
    })
    newChecklist.blocksIds = newChecklist.blocks.map(block => {
      return block.id
    })
    newChecklist.contextsIds = newChecklist.contexts.map(context => {
      return context.id
    })
    newChecklist.projectsIds = newChecklist.projects.map(project => {
      return project.id
    })
    newChecklist.categoriesIds = newChecklist.categories.map(category => {
      return category.id
    })
    newChecklist.moodsIds = newChecklist.moods.map(mood => {
      return mood.id
    })
    newChecklist.type = 1
    console.log('start checklist')
    let resp = await Client.mutate({
      mutation: CLONE_CHECKLIST,
      variables: {
        name: newChecklist.name,
        description: newChecklist.description,
        points: newChecklist.points,
        type: newChecklist.type,
        blocksIds: newChecklist.blocksIds,
        contextsIds: newChecklist.contextsIds,
        projectsIds: newChecklist.projectsIds,
        categoriesIds: newChecklist.categoriesIds,
        moodsIds: newChecklist.moodsIds,
        userId: userId
      }
    })
    return resp
  },
  async setPointStatus (pointId, status) {
    console.log('pointId', pointId)
    let resp = await Client.mutate({
      mutation: SET_POINT_STATUS,
      variables: {
        id: pointId,
        status: status
      }
    })
    return resp
  }
}
export default Checklists
