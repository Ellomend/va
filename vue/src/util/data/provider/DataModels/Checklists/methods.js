import { COMPLETED_ARRAY, PENDING_ARRAY } from '@/util/data/provider/DataModels/Constants'
import * as _ from 'lodash/collection'

export const getStats = function () {
  if (!this.points || !this.points.length) {
    return {
      completed: [],
      pending: [],
      total: 0
    }
  }
  return {
    completed: this.getCompleted(this),
    pending: this.getPending(this),
    total: this.points.length
  }
}
export const getCompleted = function () {
  if (!this.points || !this.points.length) {
    return []
  }
  let completed = this.points.filter(point => {
    if (COMPLETED_ARRAY.includes(point.status)) {
      return true
    }
  })
  return completed
}
export const getPending = function () {
  if (!this.points || !this.points.length) {
    return []
  }
  let pending = this.points.filter(point => {
    if (PENDING_ARRAY.includes(point.status)) {
      return true
    }
  })
  return pending
}
var hasFieldProperty = function (type) {
  return false
}
export const orderPointByOrder = function (checklist) {
  checklist.points = _.orderBy(checklist.points, ['order'])
  return checklist
}

export default {
  getStats,
  getCompleted,
  getPending,
  hasFieldProperty
}
