import EntityMethods from '@/util/data/provider/DataModels/BaseModel/EntityMethods'
import BaseMethods from '@/util/data/provider/DataModels/BaseModel/BaseMethods'
import { STATUSATTR } from '@/util/data/provider/DataModels/Constants'
import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'

export const PointModel = {
  ...BaseModel,
  modelName: 'point',
  ...BaseMethods,
  ...EntityMethods,
  modelMethods: {
  },
  modelAttributes: {},
  fieldAttributes: [
    STATUSATTR
  ]
}
export default PointModel
