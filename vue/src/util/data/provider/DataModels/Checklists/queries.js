import gql from 'graphql-tag'

export const USER_CHECKLISTS = gql`
    query getUserChecklists ($id: ID){
        User (id: $id) {
            checklists {
                id
                name
                description
                type
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
                points {
                    id
                    name
                    description
                    order
                    status
                }
            }
        }
    }
`
export const GET_CHECKLIST_BY_ID = gql`
    query getChecklistById ($id: ID) {
        Checklist (id: $id) {
            id
            name
            description
            type
            points {
                id
                name
                description
                order
                status
            }
        }
    }
`
export const CREATE_CHECKLIST = gql`
    mutation createChecklistMutation (
    $name: String!,
    $userId: ID!,
    $type: Int!,
    $description: String,
    $blocksIds: [ID!],
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    ) {
        createChecklist(
            name: $name,
            userId: $userId,
            description: $description,
            blocksIds: $blocksIds,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
            type: $type
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_CHECKLIST = gql`
    mutation deleteChecklistMutation ($id: ID!) {
        deleteChecklist(id: $id) {
            id
        }
    }
`
export const UPDATE_CHECKLIST = gql`
    mutation updateRoutineMutation (
    $name: String!,
    $description: String,
    $id: ID!,
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $blocksIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    $type: Int,
    ) {
        updateChecklist(
            name: $name,
            description: $description,
            id: $id,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            blocksIds: $blocksIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
            type: $type
        ) {
            id
            name
            description
        }
    }
`
export const CHECKLISTS_POINTS = gql`
    query getChecklistPoints ($id: ID){
        Checklist (id: $id) {
            id
            points (orderBy: order_ASC) {
                id
                name
                description
                order
                status
            }
        }
    }
`
export const USER_POINTS = gql`
    query getUserPoints ($id: ID){
        User (id: $id) {
            checklists {
                points {
                    id
                    name
                    description
                    order
                    status
                }
            }
        }
    }
`
export const ADD_POINT = gql`
    mutation createChecklistPointMutation (
    $name: String!,
    $description: String,
    $checklistId: ID!,
    $order: Int,
    $status: Status!
    ) {
        createPoint(
            name: $name,
            description: $description,
            checklistId: $checklistId,
            order: $order,
            status: $status
        ) {
            id
            name
            description
        }
    }
`
export const REMOVE_POINT = gql`
    mutation deleteChecklistPointMutation ($id: ID!) {
        deletePoint(id: $id) {
            id
        }
    }
`
export const GET_POINT_BY_ID = gql`
    query getPointById ($id: ID) {
        Point (id: $id) {
            id
            name
            description
            order
            status
            checklist {
                id
            }
        }
    }
`

export const UPDATE_POINT = gql`
    mutation updatePointMutation (
    $name: String!,
    $description: String,
    $id: ID!,
    $status: Status,
    $order: Int,
    ) {
        updatePoint(
            name: $name,
            description: $description,
            status: $status,
            order: $order,
            id: $id,
        ) {
            id
            name
            description
            status
            order
        }
    }
`
export const CLONE_CHECKLIST = gql`
    mutation startChecklistMutation (
    $name: String!,
    $description: String,
    $userId: ID!,
    $points: [ChecklistpointsPoint!],
    $type: Int,
    $blocksIds: [ID!],
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    ) {
        createChecklist (
            name: $name,
            description: $description,
            points: $points,
            type: $type,
            userId: $userId,
            blocksIds: $blocksIds,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds
        ) {
            id
            name
            description
        }
    }
`
export const SET_POINT_STATUS = gql`
  mutation updatePointStatusMutation (
    $id: ID!,
    $status: Status,
    ) {
        updatePoint(
            status: $status,
            id: $id,
        ) {
            id
            name
            description
            status
            order
        }
    }
`
