import Client from '@/util/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import { CREATE_TASK, DELETE_TASK, UPDATE_TASK, USER_TASKS } from '@/util/data/provider/DataModels/Tasks/queries'
import { taskFactory, tasksFactory } from '@/util/data/provider/DataModels/Tasks/functions'
import {convertRelationsToIds, modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import TaskModel from '@/util/data/provider/DataModels/Tasks/TaskModel'

export const Tasks = {
  name: 'Tasks',

  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_TASKS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.tasks, TaskModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_TASK,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        ...convertRelationsToIds(payload),
        start: payload.start,
        end: payload.end
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_TASK,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_TASK,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id,
        ...convertRelationsToIds(payload),
        start: payload.start,
        end: payload.end,
        status: payload.status
      }
    })
    return resp
  },
  taskFactory,
  tasksFactory
}
export default Tasks
