import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'
import {DATESATTR, FILTERSATTR, STATUSATTR} from '@/util/data/provider/DataModels/Constants'
import { taskFunctions } from '@/util/data/provider/DataModels/Tasks/functions'
import EntityMethods from '@/util/data/provider/DataModels/BaseModel/EntityMethods'

export const TaskModel = {
  ...BaseModel,
  ...EntityMethods,
  ...taskFunctions,
  modelName: 'task',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    FILTERSATTR,
    STATUSATTR,
    DATESATTR
  ],
  ...taskFunctions
}
export default TaskModel
