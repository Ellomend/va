import gql from 'graphql-tag'

export const USER_TASKS = gql`
    query getUserTasks ($id: ID){
        User (id: $id) {
            tasks {
                id
                name
                start
                end
                status
                description
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
            }
        }
    }
`
export const CREATE_TASK = gql`
    mutation createTaskMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $blocksIds: [ID!],
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    $start: String,
    $end: String
    ) {
        createTask(
            name: $name,
            userId: $userId,
            description: $description,
            blocksIds: $blocksIds,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
            start: $start,
            end: $end
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_TASK = gql`
    mutation deleteTaskMutation ($id: ID!) {
        deleteTask(id: $id) {
            id
        }
    }
`
export const UPDATE_TASK = gql`
    mutation updateTaskMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!,
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $blocksIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    $start: String,
    $end: String,
    $status: Status
    ) {
        updateTask(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            blocksIds: $blocksIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
            start: $start,
            end: $end,
            status: $status
        ) {
            id
            name
            description
            status
        }
    }
`
