// enum Status {
//   Pending
//   Working
//   Complete
//   Failed
//   Cancelled
// }
export const PENDING = 'Pending'
export const WORKING = 'Working'
export const COMPLETE = 'Complete'
export const FAILED = 'Failed'
export const CANCELLED = 'Cancelled'
export const COMPLETED_ARRAY = [
  COMPLETE,
  FAILED,
  CANCELLED
]
export const PENDING_ARRAY = [
  PENDING,
  WORKING
]
// Field Attributes
export const DATESATTR = 'dates'
export const FILTERSATTR = 'filters'
export const CHILDRENSATTR = 'children'
export const STATUSATTR = 'status'
export const ISENTITYATTR = 'entity'
