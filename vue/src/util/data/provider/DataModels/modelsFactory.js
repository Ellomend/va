import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'

export const modelsFactory = function (modelsArray, EntityModel) {
  if (!EntityModel) {
    EntityModel = BaseModel
  }
  return modelsArray.map(entity => {
    return {
      ...entity,
      ...EntityModel
    }
  })
}
function convertEntitiesToIds (entities) {
  if (entities && entities.length) {
    return entities.map(entity => entity.id)
  }
  return []
}
export const convertRelationsToIds = function (payload) {
  return {
    blocksIds: convertEntitiesToIds(payload.blocks),
    categoriesIds: convertEntitiesToIds(payload.categories),
    projectsIds: convertEntitiesToIds(payload.projects),
    moodsIds: convertEntitiesToIds(payload.moods),
    contextsIds: convertEntitiesToIds(payload.contexts)
  }
}
