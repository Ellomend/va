import { getUserFromStorage } from '@/config'
import Client from '@/util/data/provider/DataClient/DataClient'
import gql from 'graphql-tag'

export const Custom = {
  name: 'Custom',
  async fetchFilters () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: gql`
          query allUserFiltersQuery ($id:ID) {
              User (id: $id) {
                  blocks {
                      name
                      description
                      start
                      end
                      id
                  }
                  categories {
                      name
                      description
                      id
                  }
                  moods {
                      id
                      name
                      description
                  }
                  projects {
                      id
                      name
                      description
                  }
                  contexts {
                      id
                      name
                      description
                      color
                  }
              }
          }
      `,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return resp
  }
}
export default Custom
