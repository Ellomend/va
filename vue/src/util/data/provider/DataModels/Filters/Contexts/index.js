import { getUserFromStorage } from '@/config'
import Client from '@/util/data/provider/DataClient/DataClient'

import {
  CREATE_CONTEXT, DELETE_CONTEXT, UPDATE_CONTEXT,
  USER_CONTEXTS
} from '@/util/data/provider/DataModels/Filters/Contexts/queries'
import {modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import ContextModel from '@/util/data/provider/DataModels/Filters/Contexts/ContextModel'

export const Contexts = {
  name: 'Contexts',
  type: 'context',
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_CONTEXTS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.contexts, ContextModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_CONTEXT,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_CONTEXT,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_CONTEXT,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id
      }
    })
    return resp
  }
}
export default Contexts
