import gql from 'graphql-tag'

export const USER_CONTEXTS = gql`
    query getUserContexts ($id: ID){
        User (id: $id) {
            contexts {
                id
                name
                description
            }
        }
    }
`
export const CREATE_CONTEXT = gql`
    mutation createContextMutation (
    $name: String!,
    $userId: ID!,
    $description: String
    ) {
        createContext(
            name: $name,
            userId: $userId,
            description: $description
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_CONTEXT = gql`
    mutation deleteContextMutation ($id: ID!) {
        deleteContext(id: $id) {
            id
        }
    }
`
export const UPDATE_CONTEXT = gql`
    mutation updateContextMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!
    ) {
        updateContext(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
        ) {
            id
            name
            description
        }
    }
`
