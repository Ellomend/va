import FiltersBaseModel from '@/util/data/provider/DataModels/Filters/FiltersBaseModel'

export const ContextModel = {
  ...FiltersBaseModel,
  modelName: 'Context',
  type: 'context',
  modelMethods: {
  },
  modelAttributes: {}
}
export default ContextModel
