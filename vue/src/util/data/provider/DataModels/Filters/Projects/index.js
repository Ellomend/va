import { getUserFromStorage } from '@/config'
import Client from '@/util/data/provider/DataClient/DataClient'

import {
  CREATE_PROJECT, DELETE_PROJECT, UPDATE_PROJECT,
  USER_PROJECTS
} from '@/util/data/provider/DataModels/Filters/Projects/queries'
import {modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import ProjectModel from '@/util/data/provider/DataModels/Filters/Projects/ProjectModel'

export const Projects = {
  name: 'projects',
  type: 'project',
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_PROJECTS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.projects, ProjectModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_PROJECT,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_PROJECT,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_PROJECT,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id
      }
    })
    return resp
  }
}
export default Projects
