import FiltersBaseModel from '@/util/data/provider/DataModels/Filters/FiltersBaseModel'

export const ProjectModel = {
  ...FiltersBaseModel,
  modelName: 'Project',
  type: 'project',
  modelMethods: {
  },
  modelAttributes: {}
}
export default ProjectModel
