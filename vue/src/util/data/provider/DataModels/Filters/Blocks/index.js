import { getUserFromStorage } from '@/config'
import {modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import Client from '@/util/data/provider/DataClient/DataClient'

import {
  CREATE_BLOCK, DELETE_BLOCK, UPDATE_BLOCK,
  USER_BLOCKS
} from '@/util/data/provider/DataModels/Filters/Blocks/queries'
import BlockModel from '@/util/data/provider/DataModels/Filters/Blocks/BlockModel'

export const Blocks = {
  name: 'Blocks',
  type: 'block',
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_BLOCKS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.blocks, BlockModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_BLOCK,
      variables: {
        name: payload.name,
        description: payload.description,
        start: payload.start,
        end: payload.end,
        userId: userId
      }
    })
    return resp
  },
  async remove (payload) {
    console.log('remove block')
    let resp = await Client.mutate({
      mutation: DELETE_BLOCK,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_BLOCK,
      variables: {
        name: payload.name,
        description: payload.description,
        id: payload.id,
        start: payload.start,
        end: payload.end,
        userId: userId
      }
    })
    return resp
  }
}
export default Blocks
