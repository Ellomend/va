import gql from 'graphql-tag'

export const USER_BLOCKS = gql`
    query getUserProjects ($id: ID){
        User (id: $id) {
            blocks {
                id
                name
                description
                start
                end
            }
        }
    }
`
export const CREATE_BLOCK = gql`
    mutation createBlockMutation (
    $name: String!,
    $description: String,
    $start: String,
    $end: String
    $userId: ID
    ) {
        createBlock(
            name: $name,
            description: $description,
            start: $start,
            end: $end,
            userId: $userId
        ) {
            name
            description
            start
            end
        }
    }
`
export const DELETE_BLOCK = gql`
    mutation deleteBlockMutation ($id: ID!) {
        deleteBlock(id: $id) {
            id
        }
    }
`
export const UPDATE_BLOCK = gql`
    mutation updateBlockMutation (
    $name: String!,
    $description: String,
    $id: ID!,
    $start: String!,
    $end: String!,
    $userId: ID
    ) {
        updateBlock(
            name: $name,
            description: $description,
            id: $id,
            start: $start,
            end: $end,
            userId: $userId
        ) {
            id
            name
            description
        }
    }
`
