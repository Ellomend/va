import FiltersBaseModel from '@/util/data/provider/DataModels/Filters/FiltersBaseModel'

export const BlockModel = {
  ...FiltersBaseModel,
  modelName: 'Block',
  type: 'block',
  modelMethods: {
  },
  modelAttributes: {}
}
export default BlockModel
