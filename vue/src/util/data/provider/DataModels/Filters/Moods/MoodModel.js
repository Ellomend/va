import FiltersBaseModel from '@/util/data/provider/DataModels/Filters/FiltersBaseModel'

export const MoodModel = {
  ...FiltersBaseModel,
  modelName: 'Mood',
  type: 'mood',
  modelMethods: {
  },
  modelAttributes: {}
}
export default MoodModel
