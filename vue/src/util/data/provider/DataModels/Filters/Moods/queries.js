import gql from 'graphql-tag'

export const USER_MOODS = gql`
    query getUserMoods ($id: ID){
        User (id: $id) {
            moods {
                id
                name
                description
            }
        }
    }
`
export const CREATE_MOOD = gql`
    mutation createMoodMutation (
    $name: String!,
    $userId: ID!,
    $description: String
    ) {
        createMood(
            name: $name,
            userId: $userId,
            description: $description
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_MOOD = gql`
    mutation deleteMoodMutation ($id: ID!) {
        deleteMood(id: $id) {
            id
        }
    }
`
export const UPDATE_MOOD = gql`
    mutation updateMoodMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!
    ) {
        updateMood(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
        ) {
            id
            name
            description
        }
    }
`
