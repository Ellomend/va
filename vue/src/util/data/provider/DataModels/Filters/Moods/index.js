import { getUserFromStorage } from '@/config'
import Client from '@/util/data/provider/DataClient/DataClient'

import {
  CREATE_MOOD, DELETE_MOOD, UPDATE_MOOD,
  USER_MOODS
} from '@/util/data/provider/DataModels/Filters/Moods/queries'
import {modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import MoodModel from '@/util/data/provider/DataModels/Filters/Moods/MoodModel'

export const Moods = {
  name: 'Moods',
  type: 'mood',
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_MOODS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.moods, MoodModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_MOOD,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_MOOD,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_MOOD,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id
      }
    })
    return resp
  }
}
export default Moods
