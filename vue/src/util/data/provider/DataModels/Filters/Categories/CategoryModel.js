import FiltersBaseModel from '@/util/data/provider/DataModels/Filters/FiltersBaseModel'

export const CategoryModel = {
  ...FiltersBaseModel,
  modelName: 'category',
  type: 'category',
  modelMethods: {
  },
  modelAttributes: {}
}
export default CategoryModel
