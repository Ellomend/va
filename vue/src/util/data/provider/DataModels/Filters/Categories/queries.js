import gql from 'graphql-tag'

export const USER_CATEGORIES = gql`
    query getUserCategories ($id: ID){
        User (id: $id) {
            categories {
                id
                name
                description
            }
        }
    }
`
export const CREATE_CATEGORY = gql`
    mutation createCategoryMutation (
    $name: String!,
    $userId: ID!,
    $description: String
    ) {
        createCategory(
            name: $name,
            userId: $userId,
            description: $description
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_CATEGORY = gql`
    mutation deleteCategoryMutation ($id: ID!) {
        deleteCategory(id: $id) {
            id
        }
    }
`
export const UPDATE_CATEGORY = gql`
    mutation updateCategoryMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!
    ) {
        updateCategory(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
        ) {
            id
            name
            description
        }
    }
`
