import gql from 'graphql-tag'

export const USER_GOALS = gql`
    query getUserGoals ($id: ID){
        User (id: $id) {
            goals {
                id
                name
                start
                end
                description
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
                steps {
                    id
                    name
                    description
                    start
                    end
                    finished
                    status
                }
            }
        }
    }
`
export const CREATE_GOAL = gql`
    mutation createGoalMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $start: DateTime,
    $end: DateTime,
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $blocksIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!]
    ) {
        createGoal(
            name: $name,
            userId: $userId,
            description: $description,
            start: $start,
            end: $end,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            blocksIds: $blocksIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_GOAL = gql`
    mutation deleteGoalMutation ($id: ID!) {
        deleteGoal(id: $id) {
            id
        }
    }
`
export const UPDATE_GOAL = gql`
    mutation updateGoalMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!,
    $start: DateTime,
    $end: DateTime,
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $blocksIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!]
    ) {
        updateGoal(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
            start: $start,
            end: $end,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            blocksIds: $blocksIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
        ) {
            id
            name
            description
        }
    }
`
export const GOAL_STEPS = gql`
    query getGoalSteps ($id: ID){
        Goal (id: $id) {
            steps {
                id
                name
                description
                start
                end
                finished
                status
            }
        }
    }
`
export const GET_STEP_BY_ID = gql`
    query getStepById ($id: ID) {
        Step (id: $id) {
            id
            name
            description
            start
            end
            finished
            status
            goal {
                id
            }
        }
    }
`
export const CREATE_STEP = gql`
          mutation createStepMutation (
          $name: String!,
          $description: String,
          $start: DateTime,
          $end: DateTime
          $goalId: ID
          $status: Status!
          ) {
              createStep(
                  name: $name,
                  description: $description,
                  start: $start,
                  end: $end,
                  goalId: $goalId,
                  status: $status
              ) {
                  name
                  description
                  start
                  end
              }
          }
      `
export const REMOVE_STEP = gql`
          mutation deleteStepMutation (
          $id: ID!,
          ) {
              deleteStep(
                  id: $id,
              ) {
                  id
              }
          }
      `

export const UPDATE_STEP = gql`
          mutation updateStepMutation (
          $name: String!,
          $description: String,
          $status: Status,
          $start: DateTime,
          $end: DateTime,
          $finished: DateTime,
          $stepId: ID!
          ) {
              updateStep(
                  name: $name,
                  description: $description,
                  status: $status,
                  start: $start,
                  end: $end,
                  finished: $finished,
                  id: $stepId
              ) {
                  id
                  name
                  description
                  start
                  end
                  status
                  finished
              }
          }
      `
