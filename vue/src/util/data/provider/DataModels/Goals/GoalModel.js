import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'
import { CHILDRENSATTR, DATESATTR, FILTERSATTR } from '@/util/data/provider/DataModels/Constants'
import EntityWithDatesMethods from '@/util/data/provider/DataModels/BaseModel/EntityWithDatesMethods'

export const GoalModel = {
  ...BaseModel,
  ...EntityWithDatesMethods,
  modelName: 'goal',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    CHILDRENSATTR,
    FILTERSATTR,
    DATESATTR
  ]
}
export default GoalModel
