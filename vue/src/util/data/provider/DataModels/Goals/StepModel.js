import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'
import {DATESATTR, ISENTITYATTR, STATUSATTR} from '@/util/data/provider/DataModels/Constants'
import EntityMethods from '@/util/data/provider/DataModels/BaseModel/EntityMethods'
import EntityWithDatesMethods from '@/util/data/provider/DataModels/BaseModel/EntityWithDatesMethods'

export const StepModel = {
  ...BaseModel,
  ...EntityMethods,
  ...EntityWithDatesMethods,
  modelName: 'step',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    ISENTITYATTR,
    DATESATTR,
    STATUSATTR
  ]
}
export default StepModel
