import Client from '@/util/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import {
  CREATE_GOAL, CREATE_STEP, DELETE_GOAL, GET_STEP_BY_ID, GOAL_STEPS, REMOVE_STEP, UPDATE_GOAL, UPDATE_STEP,
  USER_GOALS
} from '@/util/data/provider/DataModels/Goals/queries'
import {convertRelationsToIds, modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import GoalModel from '@/util/data/provider/DataModels/Goals/GoalModel'
import StepModel from '@/util/data/provider/DataModels/Goals/StepModel'

export const Goals = {
  name: 'Goals',

  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_GOALS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    let goals = resp.data.User.goals.map(goal => {
      goal = {...goal}
      goal.steps = modelsFactory(goal.steps, StepModel)
      return goal
    })
    return modelsFactory(goals, GoalModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_GOAL,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        start: payload.start,
        end: payload.end,
        ...convertRelationsToIds(payload)
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_GOAL,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_GOAL,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id,
        start: payload.start,
        end: payload.end,
        ...convertRelationsToIds(payload)
      }
    })
    return resp
  },
  async fetchSteps (goal) {
    let resp = await Client.query({
      query: GOAL_STEPS,
      variables: {
        id: goal
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.Goal.steps, StepModel)
  },
  async getStepById (id) {
    let resp = await Client.query({
      query: GET_STEP_BY_ID,
      variables: {
        id: id
      }
    })
    return modelsFactory([resp.data.Step], StepModel)[0]
  },
  async createStep (payload) {
    console.log('create step', payload)
    let resp = await Client.mutate({
      mutation: CREATE_STEP,
      variables: {
        name: payload.name,
        description: payload.description,
        start: payload.start,
        end: payload.end,
        goalId: payload.goalId,
        status: payload.status
      }
    })
    return resp.data.createStep
  },
  async removeStep (payload) {
    console.log('remove step id', payload.id)
    let resp = await Client.mutate({
      mutation: REMOVE_STEP,
      variables: {
        id: payload.id
      }
    })
    return resp.data.deleteStep
  },
  async updateStep (payload) {
    let resp = await Client.mutate({
      mutation: UPDATE_STEP,
      variables: {
        stepId: payload.id,
        name: payload.name,
        description: payload.description,
        status: payload.status,
        start: payload.start,
        end: payload.end,
        finished: payload.finished
      },
      fetchPolicy: 'no-cache'
    })
    return resp.data.updateStep
  }
}
export default Goals
