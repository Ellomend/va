import later from 'later'
export const belongsToDay = function (day) {
  if (!this.schedule) {
    return false
  }
  // we really should optimise those
  let date = new Date(day.moment.format('YYYY-MM-DD'))
  let sched = later.parse.text(this.schedule)
  return later.schedule(sched).isValid(date)
}
export const getStatus = function () {
  if (!this.hasStart() || !this.hasLastComplete()) {
    return false
  }
  return true
}
export const hasStart = function () {
  return !!this.start
}
export const hasLastComplete = function () {
  return !!this.lastComplete
}

export default {
  belongsToDay,
  getStatus,
  hasLastComplete,
  hasStart
}
