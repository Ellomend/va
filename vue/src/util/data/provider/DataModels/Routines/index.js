import Client from '@/util/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import {
  CREATE_ROUTINE, DELETE_ROUTINE, UPDATE_ROUTINE, UPDATE_ROUTINE_WITH_OBJECTS,
  USER_ROUTINES, GET_ROUTINE_BY_ID, UPDATE_ROUTINE_SCHEDULE, UPDATE_ROUTINE_START, UPDATE_ROUTINE_LASTCOMPLETE
} from '@/util/data/provider/DataModels/Routines/queries'
import {modelsFactory} from '@/util/data/provider/DataModels/modelsFactory'
import { belongsToDay } from './methods'
import RoutineModel from '@/util/data/provider/DataModels/Routines/RoutineModel'
// 1
export const Routines = {
  name: 'Routines',

  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_ROUTINES,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.routines, RoutineModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_ROUTINE,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        blocksIds: payload.blocks,
        categoriesIds: payload.categories,
        projectsIds: payload.projects,
        moodsIds: payload.moods,
        contextsIds: payload.contexts,
        lastComplete: payload.lastComplete,
        schedule: payload.schedule,
        start: payload.start
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_ROUTINE,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_ROUTINE,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id,
        blocksIds: payload.blocks,
        categoriesIds: payload.categories,
        projectsIds: payload.projects,
        moodsIds: payload.moods,
        contextsIds: payload.contexts,
        lastComplete: payload.lastComplete,
        schedule: payload.schedule,
        start: payload.start
      }
    })
    return resp
  },
  async updateWithObjects (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_ROUTINE_WITH_OBJECTS,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id,
        blocksIds: payload.blocks,
        categoriesIds: payload.categories,
        projectsIds: payload.projects,
        moodsIds: payload.moods,
        contextsIds: payload.contexts,
        lastComplete: payload.lastComplete,
        schedule: payload.schedule,
        start: payload.start
      }
    })
    return resp
  },
  async getById (id) {
    let resp = await Client.query({
      query: GET_ROUTINE_BY_ID,
      variables: {
        id: id
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.Routine, RoutineModel)
  },
  // todo wtf is this
  // make fkn longer names
  // or abstract
  // raspredelit po otvetstvannast9m
  // design namespaces
  // make time
  async setSchedule (id, schedule) {
    let resp = await Client.mutate({
      mutation: UPDATE_ROUTINE_SCHEDULE,
      variables: {
        id: id,
        schedule: schedule
      }
    })
    return resp
  },
  async setStart (id, start) {
    let resp = await Client.mutate({
      mutation: UPDATE_ROUTINE_START,
      variables: {
        id: id,
        start: start
      }
    })
    return resp
  },
  async setLastComplete (id, lastComplete) {
    let resp = await Client.mutate({
      mutation: UPDATE_ROUTINE_LASTCOMPLETE,
      variables: {
        id: id,
        lastComplete: lastComplete
      }
    })
    return resp
  },
  modelMethods: {
    belongsToDay
  },
  modelAttributes: {
    type: 'routine'
  },
  modelsFactory
}
export default Routines
