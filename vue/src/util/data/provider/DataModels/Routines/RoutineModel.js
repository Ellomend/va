import BaseModel from '@/util/data/provider/DataModels/BaseModel/BaseModel'
import { FILTERSATTR } from '@/util/data/provider/DataModels/Constants'
import routineMethods from './methods'
import laterObj from '@/util/data/provider/DataModels/Routines/later'
export const RoutineModel = {
  ...BaseModel,
  ...routineMethods,
  modelName: 'routine',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    FILTERSATTR
  ],
  laterObj
}
export default RoutineModel
