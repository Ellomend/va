import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
// YOUR_GRAPH_QL_ENDPOINT_HERE
/**
 * Apollo 2
 * ApolloCLient (was ApolloClient)
 * httpLink (was createBatchingNetworkInterface)
 */
const httpLink = new HttpLink({
  // You should use an absolute URL here
  uri: 'https://api.graph.cool/simple/v1/' + process.env.API_KEY
})

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
  connectToDevTools: true
})
export const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})
// const apolloClient = new ApolloClient({
//   networkInterface: networkInterfaceWithSubscriptions,
//   dataIdFromObject: o => o.id
// })

export default apolloClient
