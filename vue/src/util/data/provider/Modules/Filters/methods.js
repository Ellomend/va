function findInList (list, selectedFilter) {
  let find = list.find(filter => {
    if (filter.type === selectedFilter.getType() && filter.id === selectedFilter.id) {
      return true
    }
  })
  return find
}

function getFilterStatus (list, filter) {
  return findInList(list, filter)
}

/**
 * Adds filter to list
 * @param list
 * @param filter
 */
function addnew (list, filter) {
  let newFilter = {
    type: filter.getType(),
    id: filter.id,
    constrain: true
  }
  list.push(newFilter)
}

function toggleStatus (filter, list) {
  list = list.map(iteratedFilter => {
    if (iteratedFilter.type === filter.type && iteratedFilter.id === filter.id) {
      iteratedFilter.constrain = !iteratedFilter.constrain
    }
    return iteratedFilter
  })
  return list
}

function removeFilter (filter, list) {
  list = list.filter(item => {
    return !(item.type === filter.getType() && item.id === filter.id)
  })
  return list
}

/**
 * Changes filter status in list or adds filter
 * There three states of filter in list can be
 * 1) filter.constrain: true - we only show entities which have this filter
 * 2) filter.constrain: false - we only show entities which don't have this filter
 * 2) filter absent in list - we do not take in account this filter at all
 * So there three cases for toggle:
 * 1) if list contains no toggledFilter - we created one with constrain = true
 * 2) if toggledFilter in list have constrain === true - we change it to false
 * 3) if toggledFilter in list have constrain === false - we change it to true
 *
 * @param filter
 * @param list
 */
export const toggleFilter = function (filter, list) {
  console.log('store before toggle', list)
  let status = getFilterStatus(list, filter)
  if (status === null || status === undefined) {
    addnew(list, filter, true)
  } else if (status.constrain === true) {
    list = toggleStatus(filter, list)
  } else {
    list = removeFilter(filter, list)
  }
  console.log('store after toggle', list)
  return list
}
