import Repository from '@/util/data/provider/DataModels/DataModelsRepository'
import {toggleFilter} from '@/util/data/provider/Modules/Filters/methods'
let repository = Repository
export const FiltersModule = {
  name: 'Filters',
  repository: Repository,
  task () {
    return this.repository.getModel(this.name)
  },
  models: {
    categories: {
      actions: {
        ...repository.getModel('Categories')
      }
    },
    moods: {
      actions: {
        ...repository.getModel('Moods')
      }
    },
    projects: {
      actions: {
        ...repository.getModel('Projects')
      }
    },
    contexts: {
      actions: {
        ...repository.getModel('Contexts')
      }
    },
    blocks: {
      actions: {
        ...repository.getModel('Blocks')
      }
    },
    custom: {
      actions: {
        ...repository.getModel('Custom')
      }
    }
  },
  actions: {
    toggleFilter
  }
}
export default FiltersModule
