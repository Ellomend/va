import Repository from '@/util/data/provider/DataModels/DataModelsRepository'
export const GoalsModule = {
  name: 'Goals',
  repository: Repository,
  goal () {
    return this.repository.getModel(this.name)
  },
  model () {
    return this.repository.getModel(this.name)
  },
  actions () {
    // todo refactor to do it with less characters or to map it to vuex directly
    let model = this.model()
    return {
      model: model,
      ...model
    }
  }
}
export default GoalsModule
