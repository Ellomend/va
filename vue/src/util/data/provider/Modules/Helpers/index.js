import * as funs from '@/util/data/provider/Modules/Helpers/helpersFunctions'

export default {
  name: 'helpers',
  ...funs
}
