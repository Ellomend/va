export const addLoader = function (context) {
  let hashKey = Date.now()
  context.dispatch('addLoader', hashKey, {root: true})
  return hashKey
}
export const removeLoader = function (context, hashKey) {
  context.dispatch('removeLoader', hashKey, {root: true})
}
import moment from 'moment'

export const getTimeValue = function (date) {
  return moment(date, 'HH:mm').isValid() ? moment(date, 'HH:mm') : moment(date, 'HH:mm:ss')
}
export const getDateValue = function (date) {
  return moment(date, 'YYYY-MM-DD')
}
export const validateBlockRange = function (start, end) {
  let momentStart = getTimeValue(start)
  let momentEnd = getTimeValue(end)
  if (!momentStart.isValid() || !momentEnd.isValid()) {
    return false
  }
  if (momentEnd.isBefore(momentStart)) {
    return false
  }
  return true
}
export const validateDateRange = function (start, end) {
  let momentStart = moment(start)
  let momentEnd = moment(end)
  if (!momentStart.isValid() || !momentEnd.isValid()) {
    return false
  }
  if (momentEnd.isBefore(momentStart)) {
    return false
  }
  return true
}
