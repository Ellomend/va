import day from '@/util/data/provider/Modules/TimeTable/day'
import ranges from '@/util/data/provider/Modules/TimeTable/ranges'
import domClasses from '@/util/data/provider/Modules/TimeTable/domClasses'
import {filterByDateRange, getOnlyWithDates} from '@/util/data/provider/Modules/TimeTable/filters/byDates'

export const timeTable = {
  name: 'timeTable',
  ranges: ranges,
  day: day,
  actions: {
    getWeekDaysModels: function () {
      let daysArray = ranges.getThisWeekDays()
      return day.getDaysModels(daysArray)
    },
    getMonthDaysModels () {
      let daysArray = ranges.getThisMonthDays()
      return day.getDaysModels(daysArray)
    }
  },
  filters: {
    filterByDateRange,
    getOnlyWithDates
  },
  domClasses: {
    ...domClasses
  }
}

export default timeTable
