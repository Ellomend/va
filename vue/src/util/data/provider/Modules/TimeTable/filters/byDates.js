/**
 * Filter entities array by start (moment) and end (moment)
 * @param array
 * @param start
 * @param end
 */
import moment from 'moment'

export const filterByDateRange = function (array, start, end) {
  return array.filter(entity => {
    return isEntityBetweenDates(entity, start, end)
  })
}
/**
 * Check if entity is between two dates (inclusive)
 * @param entity
 * @param start
 * @param end
 * @returns {boolean}
 */
export const isEntityBetweenDates = function (entity, start, end) {
  return (entity.getStartDate().isBetween(start, end, '[]') && entity.getEndDate().isBetween(start, end, '[]'))
}
export const getOnlyWithDates = function (list) {
  return list.filter(entity => {
    return (moment(entity.getStartDate()).isValid() && moment(entity.getEndDate()).isValid())
  })
}

