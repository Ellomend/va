import { addRow } from '@/util/data/provider/Modules/TimeTable/functions'

export const day = {
  getDaysModels (days) {
    let modelsArray = days.map(day => {
      return this.dayConstructor(day)
    })
    Object.defineProperty(modelsArray, 'addRow', {
      value: addRow,
      writable: false,
      enumerable: false,
      configurable: false
    })
    return modelsArray
  },
  dayConstructor (day) {
    let newDay = {
      moment: day,
      string: day.toString(),
      toString () {
        return day.toString()
      },
      lists: {
      }
    }
    return newDay
  }
}

export default day
