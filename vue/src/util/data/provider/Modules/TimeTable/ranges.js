import {
  getRangeDays, getRangeOfDates, getThisMonthDays,
  getThisWeekDays
} from '@/util/data/provider/Modules/TimeTable/functions'

export const ranges = {
  getThisMonthDays,
  getThisWeekDays,
  getRangeDays,
  getRangeOfDates
}
export default ranges
