import Repository from '@/util/data/provider/DataModels/DataModelsRepository'
import moment from 'moment'
import later from 'later'

const regex = /;[\s]?([0-9]*)$/g
export const RoutinesModule = {
  name: 'Routines',
  repository: Repository,
  routine () {
    return this.repository.getModel(this.name)
  },
  model () {
    return this.repository.getModel(this.name)
  },
  actions () {
    // todo refactor to do it with less characters or to map it to vuex directly
    let model = this.model()
    return {
      model: model,
      ...model,
      // todo wtf is this do
      setRoutineSchedule (routineId, schedule) {
        return this.model.setSchedule(routineId, schedule)
      },
      setRoutineStart (routineId, start) {
        return this.model.setStart(routineId, start)
      },
      setRoutineLastComplete (routineId, lastComplete) {
        return this.model.setLastComplete(routineId, lastComplete)
      },
      completeRoutine (routineId) {
        let now = moment().format('dddd, MMMM Do YYYY, h:mm:ss a')
        this.setRoutineLastComplete(routineId, now)
      }
    }
  },
  // todo make a facade already
  displays: {
    routineStart (routine, format = 'dddd, MMMM Do YYYY, h:mm:ss a') {
      if (!routine.start) {
        return null
      }
      return moment(routine.start).format(format)
    },
    routineStartFromNow (routine) {
      if (!routine.start) {
        return null
      }
      return moment(routine.start).fromNow()
    },
    parsedSchedule (routine) {
      if (!routine.schedule) {
        return null
      }
      let val = routine.schedule
      let txt = val.replace(regex, '')
      return later.parse.text(txt)
    },
    getLastComplete (routine) {
      return routine.lastComplete
    },
    getLastCompleteForHumans (routine, format = 'dddd, MMMM Do YYYY, h:mm:ss a') {
      return {
        date: moment(this.getLastComplete(routine)).format(format)
      }
    },
    toDo (routine) {
      if (!this.parsedSchedule(routine)) {
        return []
      }
      let afterLc = later.schedule(this.parsedSchedule(routine)).next(10, this.getLastComplete(routine))
      let res = afterLc.filter(occur => {
        if (moment(occur).isBefore(moment())) {
          return true
        }
      }).map(occur => {
        return occur
      })
      return res
    },
    isActive (routine) {
      return !!this.toDo(routine).length
    },
    getNext (routine) {
      let next = this.toDo(routine)[0]
      if (!next) {
        return {
          date: null,
          diffToNow: null,
          diffFromNow: null
        }
      }
      return {
        date: next,
        diffToNow: moment(next).toNow(),
        diffFromNow: moment(next).fromNow()
      }
    }
  }
}
export default RoutinesModule
