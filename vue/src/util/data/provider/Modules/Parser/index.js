import filterByDates from '@/util/data/provider/Modules/Parser/processors/filterByDates'
import filterByFilters from '@/util/data/provider/Modules/Parser/processors/filterByFilters'

const porocessorsList = {
  list: {
    filterByDates,
    filterByFilters
  },
  getProcessor (name) {
    return this.list[name]
  }
}

export const parser = function () {
  return {
    processors: [],
    entities: [],
    addProcessor (processorName, params) {
      let processor = porocessorsList.getProcessor(processorName)(params)
      this.processors.push(processor)
    },
    process () {
      if (!this.entities) {
        return []
      }
      for (let processor of this.processors) {
        this.entities = this.entities.filter(entity => {
          return processor.execute(entity)
        })
      }
      return this.entities
    }
  }
}
export default parser
