
const filterByDatesFunc = function (options, entity) {
  if (!entity.hasValidDates()) {
    return !!options.showDateless
  }
  if (options.showLate) {
    return (entity.getStartDate().isBefore(options.end) && entity.getEndDate().isBefore(options.end))
  }
  return (entity.getStartDate().isBetween(options.start, options.end, '[]') && entity.getEndDate().isBetween(options.start, options.end, '[]'))
}

const filterByDates = function (options) {
  return {
    options,
    execute (entity) {
      return filterByDatesFunc(options, entity)
    }
  }
}
export default filterByDates
