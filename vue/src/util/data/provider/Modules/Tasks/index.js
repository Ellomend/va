import Repository from '@/util/data/provider/DataModels/DataModelsRepository'

export const TasksModule = {
  name: 'Tasks',
  repository: Repository,
  task () {
    return this.repository.getModel(this.name)
  },
  model () {
    return this.repository.getModel(this.name)
  },
  actions () {
    // todo refactor to do it with less characters or to map it to vuex directly
    let model = this.model()
    return {
      model: model,
      ...model
    }
  },
  methods: {
  }
}
export default TasksModule
