import Repository from '@/util/data/provider/DataModels/DataModelsRepository'
import {COMPLETED_ARRAY, PENDING_ARRAY} from '@/util/data/provider/DataModels/Constants'

export const ChecklistsModule = {
  name: 'Checklists',
  repository: Repository,
  model () {
    return this.repository.getModel(this.name)
  },
  actions () {
    // todo refactor to do it with less characters or to map it to vuex directly
    let model = this.model()
    return {
      model: model,
      ...model
    }
  },
  displays: {
    getStatus (checklist) {
      if (!checklist.points || !checklist.points.length) {
        return {
          completed: [],
          pending: [],
          total: 0
        }
      }
      return {
        completed: this.getCompleted(checklist),
        pending: this.getPending(checklist),
        total: checklist.points.length
      }
    },
    getCompleted (checklist) {
      if (!checklist.points || !checklist.points.length) {
        return []
      }
      let completed = checklist.points.filter(point => {
        if (COMPLETED_ARRAY.includes(point.status)) {
          return true
        }
      })
      return completed
    },
    getPending (checklist) {
      if (!checklist.points || !checklist.points.length) {
        return []
      }
      let pending = checklist.points.filter(point => {
        if (PENDING_ARRAY.includes(point.status)) {
          return true
        }
      })
      return pending
    }
  }
}
export default ChecklistsModule
