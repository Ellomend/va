/*
 * Copyright (c) 2017.
 * Ellomend.ru
 * All rights reserved.
 */

// About writing plugins:
// https://alligator.io/vuejs/creating-custom-plugins/
// https://medium.com/@sadickjunior/creating-a-vuejs-plugin-a90c6eb17ea5
// https://vuejs.org/v2/guide/plugins.html
import Provider from './provider/provider'
const ProviderPlugin = {
  install (Vue) {
    Vue.prototype.$Provider = Provider
  }
}

export default ProviderPlugin
