module.exports = {
  NODE_ENV: '"production"',
  LATER_COV: false,
  API_KEY: JSON.stringify(process.env.API_KEY || '"NOT DEFINED"')
}
