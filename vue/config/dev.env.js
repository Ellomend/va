var merge = require('webpack-merge')
var prodEnv = require('./prod.env')
let env;

// Check to see if 'env.js' exists. If not, use default env.apiKey.
// Comment this out and simply 'require(./env.js)' if you want to force the build to fail
// when 'env.js' is missing.
try {
  console.log('try require env file')
  env = require("./env.js");
} catch (e) {
  console.log('env file require failed')
  if (e.code !== "MODULE_NOT_FOUND") {
    console.log('module not found error')
    throw e;
  }
  console.log('API KEY NOT FOUND ')
  env = {
    key: "API_KEY_NOT_FOUND"
  };
}
console.log('env:', env)
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_KEY: JSON.stringify(process.env.API_KEY || env.key)
})
