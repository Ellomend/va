/* eslint-disable */
/**
 * cypress.io docs
 * https://docs.cypress.io/guides/overview/why-cypress.html
 * examples
 * https://github.com/cypress-io/cypress-example-recipes/tree/master/examples
 *
 * kitchensink
 * https://example.cypress.io/
 * https://github.com/cypress-io/cypress-example-kitchensink
 * https://docs.cypress.io/examples/applications/kitchen-sink.html#
 *
 * Query elements
 * https://docs.cypress.io/guides/core-concepts/introduction-to-cypress.html#Cypress-is-Like-jQuery
 *
 * Interacting with elements
 * https://docs.cypress.io/guides/core-concepts/interacting-with-elements.html#Actionability
 */


// describe('My First Test', function() {
//   it('Does not do much!', function() {
//     expect(true).to.equal(true)
//   })
//   it('visit site', () => {
//     cy.visit('http://localhost:8080/#/')
//     cy.contains('.mu-ripple-wrapper')
//   })
// })

describe('Site is online', () => {
  it('visit site and find logo', () => {
    cy.visit('/')
    // assert logo is visible
    cy.get('.logo').should('be.visible')
  })
})

// todo do complete valid login/register

describe('User able to login', () => {
  it('users logs in', () => {
    cy.visit('/')
    //user not logged in
    cy.get('.loginLogoutButton').should('contain', 'Login')
    // click login button
    cy.get('.loginLogoutButton')
      .parents('button')
      .click()
    // submit prefilled form
    cy.get('.login')
      .parents('button')
      .click()
    // assert user redirected to dashboard
    cy.get('.dashboard')
      .contains('Welcome to dashboard')
  })
})
