/* eslint-disable */
describe('user should able to perform crud tasks operations', () => {

  context('tasks', () => {
    // assert list page
    it('visit site', () => {
      cy.login()
      cy.visit('/#/start/tasks')
      cy.contains('Tasks list')
    })
    // perform creating task
    it('create task', () => {
      cy.visit('/#/start/tasks')
      cy.contains('create')
        .click()
      cy.contains('create task')
      cy.findField('name')
        .type('new task')
      cy.findField('description')
        .type('new task description')
      cy.findField('start time')
        .click()
      cy.get('.mu-calendar-monthday-row')
        .contains('button.mu-day-button', 10)
        .click()
      cy.contains('Ok')
        .click()
      cy.findField('end time')
        .click()
      cy.get('.mu-calendar-monthday-row')
        .contains('button.mu-day-button', 15)
        .click()
      cy.contains('Ok')
        .click()
      cy.contains('Create')
        .click()


    })
    // assert created
    // assert task update modal
    // perform updating task
    // assert task updated
    // perform deleting page
    // assert task deleted
  })

})