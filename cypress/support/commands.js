/* eslint-disable */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })

// -- This is a login command --
Cypress.Commands.add('login', () => {
  cy.visit('/')
  //user not logged in
  cy.get('.loginLogoutButton').should('contain', 'Login')
  // click login button
  cy.get('.loginLogoutButton')
    .parents('button')
    .click()
  // submit prefilled form
  cy.get('.login')
    .parents('button')
    .click()
  // assert user redirected to dashboard
  cy.get('.dashboard')
    .contains('Welcome to dashboard')
  cy.contains('Logout')
})
Cypress.Commands.add('findField', (label) => {
  //user not logged in
  return cy.get('.mu-text-field-content')
    .contains(label)
    .siblings('input')
})
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
